#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <array>
#include <set>
#include <iomanip>


//template function
template <typename T>
T increment(T x)
{
  return x + 1;
}

template <typename T>
T sum(T from, T to, T step)
{
  T res = from;
  while((from += step) < to)
    res += from;
  return res;
}


//template class
template <typename T>
class Array2
{
  T data[2];
public:
  T& operator[](size_t i) { return data[i];}
  const T& operator[](size_t i) const { return data[i];}
  T sum() const {return data[0] + data[1];}
};

//template variable
template <typename T>
constexpr T pi = T(3.14159265358979);


std::ostream& operator<<(std::ostream &os, std::vector<int> a)
{
  for(auto el : a)
  {
    os << el << " ";
  }
  os << "\n";
  return os;
}

int main()
{
  std::cout << increment(5) <<"\n";
  std::cout << increment(4.5) <<"\n";
  char c[10] = {"123456789"};
  std::cout << *increment(c) <<"\n";

//  std::vector<int> v = {1, 2, 3};
//  std::cout << increment(v) <<"\n";

  std::cout << sum(1, 10, 3) <<"\n";
  std::cout << "***\n";
  Array2<int> iarr;
  iarr[0] = 1; iarr[1] = 2;
  std::cout << iarr.sum() <<"\n";

  Array2<std::vector<int>> varr;
  varr[0] = {1, 2, 3}; varr[1] = {4, 5, 6};
//  std::cout << varr.sum() <<"\n";

  std::vector v = {1, 2, 3};

  std::cout << std::setprecision(16) << "pi<float> = " << pi<float> << "\n";
  std::cout << std::setprecision(16) << "pi<double> = " << pi<double> << "\n";
}