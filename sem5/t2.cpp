#include <algorithm>
#include <deque>
#include <vector>
#include <iostream>
#include <array>
#include <list>
#include <iomanip>


template <typename T, size_t N>
class MyArray
{
  T data[N];
public:
  T& operator[](size_t i)
  {
    if(i >= N) throw std::out_of_range("index out of range");
    return data[i];
  }
  const T& operator[] (size_t i)const
  {
    if(i >= N) throw std::out_of_range("index out of range");
    return data[i];
  }
};

template <template<typename> class Out, template<typename> class In, typename T>
Out<T> convert(const In<T>& in)
{
  Out<T> out;
  for(auto& x: in)
  {
    out.push_back(x);
  }
  return out;
}

template <typename U, typename V>
V convert2(const U& in)
{
  V out;
  for(auto& x: in)
  {
    out.push_back(x);
  }
  return out;
}

template <typename T>
void print_container(const T& cont)
{
  for(auto& x: cont)
  {
    std::cout << x << "; ";
  }
  std::cout << std::endl;
}

int main()
{
  MyArray<int, 5> a;
  for(int i =0 ; i < 5; ++i)
  {
    a[i] = i + 1;
  }
  for(int i =0 ; i < 5; ++i)
  {
    std::cout << a[i] << " ";
  }
  std::cout << "\n";
//  int q = a[10];
//  MyArray<int, a[1]> a2;

  std::vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  std::cout << "vector: ";
  print_container(vec);

  auto deq = convert<std::deque, std::vector, int>(vec);
  std::cout << "deque: ";
  print_container(deq);
}