#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <array>
#include <set>



int main()
{
  constexpr int SIZE = 10;
  std::array<int, SIZE> arr= {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  std::array<int, SIZE>::iterator i1 = arr.begin(); //end()
  std::array<int, SIZE>::const_iterator i2 = arr.cbegin();

  std::array<int, SIZE>::reverse_iterator i3 = arr.rbegin();
  std::array<int, SIZE>::const_reverse_iterator i4 = arr.crbegin();

  // [begin, end), end - указывает за границы контейнера (за последний элемент)
  // итератор и ссылка - итератор можно перемещать на другие эл-ты, может указывать в никуда


//  for(auto ii = arr.rbegin(); ii != arr.rend(); ++ii)
//  {
//    std::cout << *ii << "\n";
//  }
  std::sort(std::rbegin(arr), std::rend(arr));
  for(auto ii = arr.begin(); ii != arr.end(); ++ii)
  {
    std::cout << *ii << "\n";
  }
  std::cout << "***\n";
  for(auto ii = std::next(arr.begin(), 3); ii != std::prev(arr.end(), 3); std::advance(ii, 1))
  {
    std::cout << *ii << "\n";
  }
  for(auto ii = arr.begin()+ 3; ii != arr.end() - 3; ++ii)
  {
    std::cout << *ii << "\n";
  }

  std::set<int> s = {1 , 2 , 3 ,4, 5, 6 , 7, 8 ,9 , 10};
  auto set_it = s.begin();
  for(auto ii = std::next(s.begin(), 1); ii != std::prev(s.end(), 3); std::advance(ii, 1))
  {
    std::cout << *ii << "\n";
  }

  for(auto ii = ++s.begin(); ii != --s.end(); ++ii)
  {
    std::cout << *ii << "\n";
  }

  std::vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::cout << "***\n";
//  auto res = std::remove_if(vec.begin(), vec.end(), [](int a){ return a < 5; });
//  for(auto el = vec.begin(); el != res; ++el)
//  {
//    std::cout << *el << "; ";
//  }
//  std::cout << "\n";
//  std::cout << *res << "\n";

  //inserter, back_inserter
  int step = 4;
  for(auto ii = s.begin(); std::distance(ii, s.end()) > 0;  std::advance(ii, step))
  {
    std::cout << *ii << "; ";
  }


}