#include <iostream>
#include <vector>

// использование шаблонных типов как параметров другого шаблона (std::pair)

template <typename U, typename V>
auto swap_pair (const std::pair<U, V> &x)
{
  return std::pair<V, U>(x.second, x.first);
}

template <typename T>
auto swap_pair2 (const T &x)
{
  return std::make_pair(x.second, x.first);
}

template <typename T>
auto print_pair (const T &x)
{
  std::cout << "(" << x.first << ", " << x.second << ")\n";
}


int main()
{
  std::pair<int, std::string> p1 = {11, "wow"};
  std::pair<std::string, std::string> p2 = {"wow", {'1', '2', '3'}};

  auto p1_1 = swap_pair(p1);
  print_pair(p1_1);
  auto p2_1 = swap_pair(p2);
  print_pair(p2_1);

//  swap_pair(p1.first);
//  swap_pair2(p1.first);

  return 0;
}

