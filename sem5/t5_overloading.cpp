#include <iostream>

void f(int i) { std::cout << "f(int)" << std::endl; }
void f(long i) { std::cout << "f(long)" << std::endl; }
//template <typename T> void f(T i) { std::cout << "f(T)" << std::endl; }
template <typename T> void f(T* i) { std::cout << "f(T*)" << std::endl; }
template <typename T> void f(T&& i) { std::cout << "f(T&&)" << std::endl; }


void g(int i) { std::cout << "g(int)" << std::endl; }
void g(double i) { std::cout << "g(double)" << std::endl; }

template <typename T> void g(T&& i) { std::cout << "g(T&&)" << std::endl; }

struct A
{
  int a; int b;
};

int main() {
  f(5);
  f(5l);
  int i = 0;
  f(&i);
  f(NULL);
//  f(nullptr);
  void* p = &i;
  f(p);

  A a{};
  f(&a);
  f(a);

  int j = 10;
  float x = 3.14f;
  g(i);
  g(x);

}
