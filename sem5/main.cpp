#include <vector>
#include <iostream>


template <typename T>
class LinkedList {
public:
  struct Node
  {
    T value;
    Node* next = nullptr;
  };

  ~LinkedList()
  {
    auto ptr = head;
    while (ptr != nullptr)
    {
      auto tmp = ptr->next;
      delete ptr;
      ptr = tmp;
    }
  }

  void PushFront(const T& value)
  {
    Node* new_head = new Node;
    new_head->value = value;
    new_head->next = head;
    head = new_head;
  }

  void InsertAfter(Node* node, const T& value)
  {
    if(node == nullptr)
      PushFront(value);
    else
    {
      Node *inserted = new Node;
      inserted->value = value;
      inserted->next = node->next;
      node->next = inserted;
    }
  }

  void RemoveAfter(Node* node)
  {
    if(node == nullptr)
    {
      PopFront();
    }
    else
    {
      if(node->next == nullptr)
        return;

      auto tmp = node->next->next;
      delete node->next;
      node->next = tmp;
    }
  }

  void PopFront()
  {
    if(head == nullptr)
      return;

    auto tmp = head->next;
    delete head;
    head = tmp;
  }

  Node* GetHead() { return head; }
  const Node* GetHead() const { return head; }

private:
  Node* head = nullptr;
};

template <typename T>
std::vector<T> ToVector(const LinkedList<T>& list) {
  std::vector<T> result;
  for (auto node = list.GetHead(); node; node = node->next) {
    result.push_back(node->value);
  }
  return result;
}

void PushFront() {
  LinkedList<int> list;

  list.PushFront(1);
  list.PushFront(2);
  list.PushFront(3);

  auto v = ToVector(list);
  for(auto &elem : v)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

void InsertAfter() {
  LinkedList<std::string> list;

  list.PushFront("a");
  auto head = list.GetHead();
  std::cout << head->value << " == a\n";
  list.InsertAfter(head, "b");
  list.InsertAfter(head, "c");

  auto v = ToVector(list);
  for(auto &elem : v)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

void RemoveAfter() {
  LinkedList<int> list;
  for (int i = 1; i <= 5; ++i)
  {
    list.PushFront(i);
  }

  auto next_to_head = list.GetHead()->next;
  list.RemoveAfter(next_to_head); // удаляем 3
  list.RemoveAfter(next_to_head); // удаляем 2

  auto v = ToVector(list); //5, 4, 1
  for(auto &elem : v)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

void PopFront() {
  LinkedList<int> list;

  for (int i = 1; i <= 5; ++i) {
    list.PushFront(i);
  }
  for (int i = 1; i <= 5; ++i) {
    list.PopFront();
  }
  if(list.GetHead() != nullptr)
  {
    std::cout << "PopFront test failed\n";
  }
}

int main()
{
  PushFront();
  InsertAfter();
  RemoveAfter();
  PopFront();
  return 0;
}