#include <iostream>
#include <vector>

// Примеры на выведение одного или нескольких типов шаблона

template <typename T>
T half (T x)
{
  return x/2;
}

template <typename U, typename V>
U half2 (V x)
{
  return x/2.0;
}


template <typename T>
T Max (T x, T y)
{
  return x > y ? x : y;
}

template <typename T>
T decrement(T* ptr)
{
  return --(*ptr);
}

int main()
{
  std::cout << half<int>(5) << "\n";
  std::cout << half<double>(5) << "\n";

  auto x = half(5);
  auto y = half(5.0);

  long l = 12345l;
  unsigned int u = 1234u;
  auto val = half(l + u);

  std::cout << half2<int>(5.0) << "\n";
  std::cout << half2<double>(5) << "\n";

//  std::cout << Max(7L, 11) << "\n";
  std::cout << Max<long>(7L, 11) << "\n";

  std::cout << decrement(&u) << "\n";

  return 0;
}
