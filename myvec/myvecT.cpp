#include <iostream>


struct A
{
  std::string name;
  int id = 0;
  
  A(const std::string& a_str = "none", int a_id = 1) : name(a_str), id(a_id)
  {
//    std::cout << "A(string, int)\n";
  }
};

std::ostream& operator <<(std::ostream& os, const A& a)
{
  std::cout << "(" << a.name << ", " << a.id << ")";
  return os;
}

template <typename T>
class SimpleVector
{
public:

  SimpleVector() : data(nullptr), size(0), capacity(0)
  {
    std::cout <<"Default ctor\n";
  }

  SimpleVector(const SimpleVector& other) : SimpleVector()
  {
    std::cout <<"Copy ctor\n";
    if(this->capacity < other.capacity)
    {
      this->capacity = other.capacity;
      ReAlloc();
    }
    size = other.size;
    std::copy(other.data, other.data + other.size, data);
  }

  // copy - and - swap
  // a = b
  SimpleVector& operator=(SimpleVector other) //SimpleVector&& other
  {
    swap(*this, other);
    std::cout <<"Copy assignment\n";
    return *this;
  }

  friend void swap(SimpleVector& first, SimpleVector& second)
  {
    using std::swap;

    swap(first.size, second.size);
    swap(first.capacity, second.capacity);
    swap(first.data, second.data);
  }

  //data, size, capacity
  SimpleVector(SimpleVector&& other) noexcept
      : SimpleVector()
  {
    std::cout <<"Move ctor\n";
    swap(*this, other);
  }

  explicit SimpleVector(size_t a_size) : SimpleVector()
  {
    size = a_size;
    data = new T[2 * size]{};
    capacity = 2 * size;

    std::cout <<"Param ctor\n";
  }

  SimpleVector(size_t a_size, T def_val) : SimpleVector(a_size)
  {
    for(int i = 0; i < size; ++i)
    {
      data[i] = def_val;
    }

    std::cout <<"Param ctor 2\n";
  }

  ~SimpleVector()
  {
    std::cout <<"dtor\n";
    delete [] data;
  }

  T& operator[](size_t index)
  {
    return data[index];
  }

  const T& operator[](size_t index) const
  {
    return data[index];
  }

  T Back() const
  {
    return *(data + size - 1);
  }

  T PopBack()
  {
    T res = Back();
    --size;
    return res;
  }

  void PushBack(T value)
  {
    if(size == capacity)
    {
      ReAlloc();
    }
    *(data + size) = value;
    size++;
  }

  size_t Size() const
  {
    return size;
  }

  size_t Capacity() const
  {
    return capacity;
  }

private:
  T* data;// = nullptr;
  size_t size;// = 0;
  size_t capacity;// = 0;

  void ReAlloc()
  {
    if(capacity == 0)
    {
      data = new T [1];
      capacity = 1;
      std::cout <<"Realloc 1\n";
    }
    else
    {
      std::cout <<"Realloc 2\n";
      T *new_alloc = new T[capacity * 2];
      if(data != nullptr)
      {
        std::move(data, data + size, new_alloc);
      }
      capacity *= 2;
      data = new_alloc;
    }
  }
};

template <typename T>
void printVec(const std::string &msg, const SimpleVector<T>& vec)
{
  std::cout << msg << " : ";
  for(int i = 0; i < vec.Size(); ++i)
  {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

int main()
{
  constexpr int bb = 1;
//  A a;
  SimpleVector<A> myvec; //default ctor
  myvec.PushBack(A("10"));
  myvec.PushBack(A("20"));
  myvec.PushBack(A("30"));
  myvec.PushBack(A("40"));

  printVec("myvec", myvec);

  SimpleVector<float> myvec2(5);
  for(int i = 0; i < 5; ++i)
  {
    myvec2[i] = i + 1.50f;
  }

  printVec("myvec2", myvec2);

  SimpleVector myvec3(10, A("100"));
  printVec("myvec3", myvec3);

  SimpleVector myvec4(myvec);
  printVec("myvec4", myvec4);
  auto oldSize = myvec4.Size();
  for(int i = 0; i < oldSize; ++i)
  {
    std::cout << "PopBack()" << myvec4.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec4 after PopBack()", myvec4);

  SimpleVector<A> myvec5;
  myvec5 = myvec;
  printVec("myvec5", myvec5);

  const SimpleVector myvec_c(myvec3);

  SimpleVector myvec6(std::move(myvec3)); // SimpleVector -> SimpleVector&&
  printVec("myvec6", myvec6);

  SimpleVector myvec7{SimpleVector(5, A("5"))};
  printVec("myvec7", myvec7);
  for(int i = 0; i < myvec7.Size() - 2; ++i)
  {
    std::cout << "PopBack()" << myvec7.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec7 after PopBack()", myvec7);
}
