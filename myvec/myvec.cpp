#include <iostream>

#define PLACEMENT_NEW

struct A
{
  std::string name;
  int id = 0;
  int *ptr;
  
  A(const std::string& a_str = "none", int a_id = 1) : name(a_str), id(a_id)
  {
    std::cout << "A(string, int)\n";
    ptr = new int;
  }
  A(const A& other)
  {
    name = other.name;
    id = other.id;
    ptr = new int;
    *ptr = *other.ptr;
  }
  A& operator=(const A& other)
  {
    if(&other == this)
      return *this;

    name = other.name;
    id = other.id;
    ptr = new int;
    *ptr = *other.ptr;

    return *this;
  }
  ~A()
  {
    delete ptr;
  }


};

std::ostream& operator <<(std::ostream& os, const A& a)
{
  std::cout << "(" << a.name << ", " << a.id << ")";
  return os;
}

class SimpleVectorA
{
public:

  SimpleVectorA() : data(nullptr), size(0), capacity(0)
  {
    std::cout <<"Default ctor\n";
  }

  SimpleVectorA(const SimpleVectorA& other) : SimpleVectorA()
  {
    std::cout <<"Copy ctor\n";
    if(this->capacity < other.capacity)
    {
      this->capacity = other.capacity;
      ReAlloc();
    }
#ifdef PLACEMENT_NEW
    for(int j = 0; j < other.size; ++j)
      PushBack(other.data[j]);
#else
    size = other.size;
    std::copy(other.data, other.data + other.size, data);
#endif

  }

  // copy - and - swap
  // a = b
  SimpleVectorA& operator=(SimpleVectorA other) //SimpleVectorA&& other
  {
    swap(*this, other);
    std::cout <<"Copy assignment\n";
    return *this;
  }

  friend void swap(SimpleVectorA& first, SimpleVectorA& second)
  {
    using std::swap;

    swap(first.size, second.size);
    swap(first.capacity, second.capacity);
    swap(first.data, second.data);
  }

  //data, size, capacity
  SimpleVectorA(SimpleVectorA&& other) noexcept
      : SimpleVectorA()
  {
    std::cout <<"Move ctor\n";
    swap(*this, other);
  }

  explicit SimpleVectorA(size_t a_size, A def_val = A("default")) : SimpleVectorA()
  {
#ifdef PLACEMENT_NEW
    data = static_cast<A*>(::operator new(sizeof(A) * 2 * a_size));
#else
    data = new A[2 * a_size];
#endif

    capacity = 2 * a_size;
    for(int i = 0; i < a_size; ++i)
    {
      PushBack(def_val);
    }

    std::cout <<"Param ctor\n";
  }

  ~SimpleVectorA()
  {
    std::cout <<"dtor\n";
#ifdef PLACEMENT_NEW
    for(int i = 0; i < size; ++i)
      (data + i)->~A();
    ::operator delete(data);
#else
    delete [] data;
#endif
  }

  A& operator[](size_t index)
  {
    return data[index];
  }

  const A& operator[](size_t index) const
  {
    return data[index];
  }

  A Back() const
  {
    return *(data + size - 1);
  }

  A PopBack()
  {
    A res = Back();
    --size;
#ifdef PLACEMENT_NEW
    (data + size)->~A();
#endif
    return res;
  }

  void PushBack(A value)
  {
    if(size == capacity)
    {
      ReAlloc();
    }
#ifdef PLACEMENT_NEW
    new(data + size) A(value);
#else
    *(data + size) = value;
#endif
    size++;
  }

  size_t Size() const
  {
    return size;
  }

  size_t Capacity() const
  {
    return capacity;
  }

private:
  A* data;// = nullptr;
  size_t size;// = 0;
  size_t capacity;// = 0;

  void ReAlloc()
  {
    if(capacity == 0)
    {
#ifdef PLACEMENT_NEW
      data = static_cast<A*>(::operator new(sizeof(A)));
#else
      data = new A [1];
#endif
      capacity = 1;
      std::cout <<"Realloc 1\n";
    }
    else
    {
      std::cout <<"Realloc 2\n";
#ifdef PLACEMENT_NEW
      A *new_alloc = static_cast<A*>(::operator new(sizeof(A) * capacity * 2));
#else
      A *new_alloc = new A[capacity * 2];
#endif
      if(data != nullptr)
      {
#ifdef PLACEMENT_NEW
        for(int j = 0; j < size; ++j)
        {
          new(new_alloc + j) A(data[j]);
          (data + j)->~A();

        }
        ::operator delete(data);
#else
        std::move(data, data + size, new_alloc);
#endif
      }
      capacity *= 2;
      data = new_alloc;
    }
  }
};


void printVec(const std::string &msg, const SimpleVectorA& vec)
{
  std::cout << msg << " : ";
  for(int i = 0; i < vec.Size(); ++i)
  {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

int main()
{
  constexpr int bb = 1;
//  A a;
  SimpleVectorA myvec; //default ctor
  myvec.PushBack(A("10"));
  myvec.PushBack(A("20"));
  myvec.PushBack(A("30"));
  myvec.PushBack(A("40"));

  printVec("myvec", myvec);

//  SimpleVectorA myvec2(10);
//  printVec(myvec2);
  SimpleVectorA myvec3(10, A("100"));
  printVec("myvec3", myvec3);
//
//  SimpleVectorA myvec3_2 = SimpleVectorA(2, 3); // SimpleVectorA myvec3_2(2, 3)
//  printVec(myvec3_2);
  SimpleVectorA myvec4(myvec);
  printVec("myvec4", myvec4);
//  std::cout << "myvec4.Size() = " << myvec4.Size() << "\n";
  auto oldSize = myvec4.Size();
  for(int i = 0; i < oldSize; ++i)
  {
    std::cout << "PopBack()" << myvec4.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec4 after PopBack()", myvec4);

//  if((myvec4 = myvec3).Size() != 0)
//    printVec(myvec4);
  SimpleVectorA myvec5;
  myvec5 = myvec;
  printVec("myvec5", myvec5);

  const SimpleVectorA myvec_c(myvec3);
//  printVec(myvec_c);

  SimpleVectorA myvec6(std::move(myvec3)); // SimpleVectorA -> SimpleVectorA&&
  printVec("myvec6", myvec6);
//  std::string a = "abc";
//  std::string b = a + "123";

  // lvalue
  SimpleVectorA myvec7{SimpleVectorA(5, A("5"))};
  printVec("myvec7", myvec7);
  for(int i = 0; i < myvec7.Size() - 2; ++i)
  {
    std::cout << "PopBack()" << myvec7.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec7 after PopBack()", myvec7);
}
