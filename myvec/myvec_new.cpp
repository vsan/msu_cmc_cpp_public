#include <iostream>
#include <cstddef>
#include <cstdint>
#include <utility>
#include <memory>


template <typename T>
struct MyAlloc
{
  T* mem = nullptr;
  size_t capacity = 0;

  MyAlloc() = default;

  explicit MyAlloc(size_t a_capacity) : mem(Allocate(a_capacity)), capacity(a_capacity)
  {

  }

  MyAlloc(const MyAlloc<T>&) = delete;

  MyAlloc<T>& operator=(const MyAlloc<T>& other) = delete;

  MyAlloc(MyAlloc<T>&& other) noexcept
  {
    swap(other);
  }

  ~MyAlloc()
  {
    Free(mem);
  }

  friend void swap(MyAlloc<T>& first, MyAlloc<T>& second)
  {
    using std::swap;
    swap(first.mem, second.mem);
    swap(first.capacity, second.capacity);
  }

  MyAlloc<T>& operator=(MyAlloc<T>&& other) noexcept
  {
    if(this != &other)
      swap(*this, other);

    return *this;
  }

  const T& operator[](size_t idx) const
  {
    return mem[idx];
  }

  T& operator[](size_t idx)
  {
    return mem[idx];
  }

  const T* operator+(size_t idx) const
  {
    return mem + idx;
  }

  T* operator+(size_t idx)
  {
    return mem + idx;
  }

  T* Allocate(size_t count)
  {
    return static_cast<T*>(::operator new(count * sizeof(T)));
  }

  void Free(T* data)
  {
    ::operator delete(data);
  }
};



template < typename T>
class MyVector
{
public:

  MyVector() = default;

  MyVector(const MyVector& other) : data(other.m_size)
  {
    for(size_t i = 0; i < other.m_size; ++i)
    {
      new(data + i) T(other.data[i]);
//      data[i] = other.data[i];
    }
    m_size = other.m_size;
  }

  // copy - and - swap
  // a = b
  MyVector& operator=(MyVector other) //MyVector&& other
  {
    swap(*this, other);
    return *this;
  }

  friend void swap(MyVector& first, MyVector& second)
  {
    using std::swap;

    swap(first.data, second.data);
    swap(first.m_size, second.m_size);
  }

  MyVector(MyVector&& other) noexcept
  {
    swap(*this, other);
  }

  explicit MyVector(size_t n)
  {
    data = MyAlloc<T>(n);
    m_size = n;
  }

  explicit MyVector(size_t n, T default_val)
  {
    data = MyAlloc<T>(n);
    for(size_t i = 0; i < n; ++i)
      new(data + i) T(default_val);
//      data[i] = default_val;
    m_size = n;
  }

  ~MyVector()
  {
    for(size_t i = 0; i < m_size; ++i)
      data[i].~T();
  }

  void Reserve(size_t n)
  {
    if(n > data.capacity)
    {
      auto data_new = MyAlloc<T>(n);

      for(size_t i = 0; i < m_size; ++i)
      {
        new(data_new + i) T(std::move(data[i]));
//        data_new[i] = std::move(data[i]);
      }
      swap(data, data_new);
    }

  }

  T& operator[](size_t index)
  {
    return data[index];
  }

  const T& operator[](size_t index) const
  {
    return data[index];
  }

  T Back() const
  {
    return *(data + m_size - 1);
  }

  T PopBack()
  {
    T res = Back();
    --m_size;
    data[m_size].~T();
    return res;
  }

  void PushBack(const T& value)
  {
    if(m_size == data.capacity)
    {
      Reserve(m_size == 0 ? 1 : 2 * m_size);
    }
//    data[m_size] = value;
    new(data + m_size) T(value);
    ++m_size;
  }

  template <typename ... Args>
  T& EmplaceBack(Args&&... args)
  {
    if(m_size == data.capacity)
    {
      Reserve(m_size == 0 ? 1 : 2 * m_size);
    }
    new (data + m_size) T(std::forward<Args>(args)...);
    ++m_size;
    return data[m_size - 1];
  }

  void PushBack(T&& value)
  {
    if(m_size == data.capacity)
    {
      Reserve(m_size == 0 ? 1 : 2 * m_size);
    }
    new(data + m_size) T(std::move(value));
//    data[m_size] = std::move(value);
    ++m_size;
  }

  size_t Size() const noexcept
  {
    return m_size;
  }

  size_t Capacity() const noexcept
  {
    return data.capacity;
  }

  T* begin() noexcept
  {
    return data.mem;
  }

  T* end() noexcept
  {
    return data + m_size;
  }

  const T& begin() const noexcept
  {
    return data.mem;
  }

  const T& end() const noexcept
  {
    return data + m_size;
  }

  const T& cbegin() const noexcept
  {
    return data.mem;
  }

  const T& cend() const noexcept
  {
    return data + m_size;
  }

private:
  MyAlloc<T> data;
  size_t m_size = 0;

};


struct A
{
  std::string name;
  int id = 0;

  A(const std::string& a_str = "none", int a_id = 1) : name(a_str), id(a_id)
  {
    std::cout << "A(string, int)\n";
  }
};

struct NonCopyable
{
  std::string name;
  int id = 0;

  NonCopyable(NonCopyable& other) = delete;
  NonCopyable& operator=(NonCopyable& other) = delete;
};

std::ostream& operator <<(std::ostream& os, const A& a)
{
  std::cout << "(" << a.name << ", " << a.id << ")";
  return os;
}

template <typename T>
void printVec(const std::string &msg, const MyVector<T>& vec)
{
  std::cout << msg << " : ";
  for(size_t i = 0; i < vec.Size(); ++i)
  {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

int main()
{
  MyVector<A> myvec;
  myvec.PushBack(A("10"));
  myvec.PushBack(A("20"));
  myvec.PushBack(A("30"));
  myvec.PushBack(A("40"));
  printVec("myvec", myvec);

  MyVector<A> myvec1;
  myvec1.EmplaceBack("10", 10);
  myvec1.EmplaceBack("20", 20);
  myvec1.EmplaceBack("30", 30);
  myvec1.EmplaceBack("40", 40);
  printVec("myvec1", myvec1);

  MyVector<int> myvec2(10);
  myvec2[0] = 11;
  myvec2[8] = 88;
  printVec("myvec2", myvec2);


  MyVector myvec3(10, A("100"));
  printVec("myvec3", myvec3);

  MyVector myvec4(myvec);
  printVec("myvec4", myvec4);

  auto oldSize = myvec4.Size();
  for(int i = 0; i < oldSize; ++i)
  {
    std::cout << "PopBack()" << myvec4.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec4 after PopBack()", myvec4);


  MyVector<A> myvec5;
  myvec5 = myvec;
  printVec("myvec5", myvec5);

  MyVector myvec6(std::move(myvec3));
  printVec("myvec6", myvec6);

  MyVector myvec7{MyVector(5, A("5"))};
  printVec("myvec7", myvec7);
  for(int i = 0; i < myvec7.Size() - 2; ++i)
  {
    std::cout << "PopBack()" << myvec7.PopBack() << ", ";
  }
  std::cout << "\n";
  printVec("myvec7 after PopBack()", myvec7);
}