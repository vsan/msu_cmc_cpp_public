#include <string>
#include <cassert>
#include <memory>
#include <vector>
#include <iostream>
#include <thread>
#include <unordered_set>
#include <chrono>
#include <random>
#include <sstream>
#include <future>

struct Res
{
  explicit Res(const std::string &d) : data(d)
  {
    std::cout << "Resource()\n";
  }

  ~Res()
  {
    std::cout << "~Resource()\n";
  }

  const std::string& GetRes() const
  {
    return data;
  }

  std::string& GetRes()
  {
    return data;
  }
private:
  std::string data;
};

int getRandomTime()
{
  static std::random_device r;
  static std::default_random_engine e1(r());
  static std::uniform_int_distribution<int> uniform_dist(100, 500);

  return uniform_dist(e1);
}

void DoWork(std::shared_ptr<Res> ptr)
{
  std::stringstream ss;
  ss << "[tid:" << std::this_thread::get_id() << "]: " << ptr->GetRes() << "; use_count = "
     << ptr.use_count() << "\n";

  std::cout << ss.str();
}

std::vector<std::future<void>> CreateWork()
{
  std::vector<std::future<void>> work;
  work.reserve(128);

  auto res = std::make_shared<Res>("VeryImportantResource");

  for(int i = 0; i < 128; ++i)
  {
    work.push_back(std::async(DoWork, res));
  }
  return work;
}

int main()
{
  auto r = CreateWork();
}

