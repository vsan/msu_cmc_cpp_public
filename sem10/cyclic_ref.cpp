#include <string>
#include <cassert>
#include <memory>

struct Cat;

struct Person
{
  std::shared_ptr<Cat> cat;
};

struct Cat
{
//  std::shared_ptr<Person> owner;
  std::weak_ptr<Person> owner;
//  Person* owner;
};


int main()
{
  auto person = std::make_shared<Person>();
  auto cat = std::make_shared<Cat>();

  person->cat = cat;
  cat->owner  = person;
}
