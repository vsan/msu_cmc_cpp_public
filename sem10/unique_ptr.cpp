#include <cassert>
#include <utility>
#include <iostream>

template <typename T>
class UniquePtr
{
private:
  T* data;

public:
  UniquePtr(const UniquePtr&) = delete;
  UniquePtr& operator = (const UniquePtr&) = delete;

  UniquePtr(): data(nullptr) {}
  UniquePtr(std::nullptr_t): data(nullptr) {}
  UniquePtr(T * ptr): data(ptr) {}
  UniquePtr(UniquePtr&& other) noexcept : data(other.Release()) {}
  UniquePtr& operator = (std::nullptr_t);
  UniquePtr& operator = (UniquePtr&& other) noexcept;
  ~UniquePtr() { Reset(); }

  T& operator * () const { return *data; }

  T * operator -> () const { return data; }

  T * Release();

  void Reset(std::nullptr_t = nullptr);
  void Reset(T * ptr);

  void Swap(UniquePtr& other);

  T * Get() const;
};

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator = (std::nullptr_t)
{
  Reset();
  return *this;
}

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator = (UniquePtr&& other) noexcept
{
  UniquePtr<T>(other.Release()).Swap(*this);
  return *this;
}

template <typename T>
T * UniquePtr<T>::Release()
{
  T * tmp = data;
  data = nullptr;
  return tmp;
}

template <typename T>
void UniquePtr<T>::Reset(std::nullptr_t)
{
  T * tmp = data;
  data = nullptr;
  if (tmp) {
    delete tmp;
  }
}

template <typename T>
void UniquePtr<T>::Reset(T * ptr)
{
  T * tmp = data;
  data = ptr;
  if (tmp) {
    delete tmp;
  }
}

template <typename T>
void UniquePtr<T>::Swap(UniquePtr& other)
{
  swap(data, other.data);
}

template <typename T>
T * UniquePtr<T>::Get() const
{
  return data;
}

template <typename T, typename ... Args>
UniquePtr<T> MakeUnique(Args&& ... args)
{
  UniquePtr<T> res(new T(std::forward<Args>(args)...));
  return res;
}

struct Item
{
  static int counter;
  int value;
  Item(int v = 0): value(v)
  {
    ++counter;
  }
  Item(const Item& other): value(other.value)
  {
    ++counter;
  }
  ~Item()
  {
    --counter;
  }
};

int Item::counter = 0;


void TestLifetime()
{
  Item::counter = 0;
  {
    UniquePtr<Item> ptr(new Item);

    assert(Item::counter == 1);

    ptr.Reset(new Item);
    assert(Item::counter == 1);
  }
  assert(Item::counter == 0);

  {
    UniquePtr<Item> ptr(new Item);
    assert(Item::counter == 1);

    assert(ptr.Get() != nullptr);
    auto releasedRawPtr = ptr.Release();
    assert(ptr.Get() == nullptr);
    assert(Item::counter == 1);

    delete releasedRawPtr;
    assert(Item::counter == 0);
  }
  assert(Item::counter == 0);
  std::cout << "TestLifetime OK\n";
}

void TestGetters()
{
  UniquePtr<Item> ptr(new Item(42));
  assert(ptr.Get()->value == 42);
  assert((*ptr).value == 42);
  assert(ptr->value == 42);

  std::cout << "TestGetters OK\n";
}

struct A
{
  int a, b, c;
  std::string str;
  A(int a_, int b_, int c_, const std::string &str_) : a(a_), b(b_), c(c_), str(str_){}
};

void TestMakeUnique()
{
  {
    auto ptr = MakeUnique<Item>(42);
    assert(ptr->value == 42);
  }

  {
    auto ptr = MakeUnique<A>(1, 2, 3, "hello");
    assert(ptr->str == "hello");
    assert(ptr->b == 2);
  }
  std::cout << "TestMakeUnique OK\n";
}

void TestGettersNullptr()
{
  UniquePtr<Item> ptr;
  assert(ptr.Get() == nullptr);
  std::cout << "TestGettersNullptr OK\n";
}

int main()
{
  TestLifetime();
  TestMakeUnique();
  TestGetters();
  TestGettersNullptr();
}
