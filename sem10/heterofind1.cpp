#include <string>
#include <cassert>
#include <memory>
#include <vector>
#include <iostream>
#include <set>

template <typename T>
struct A
{
  void insertPtr(T* ptr)
  {
    s.insert(std::unique_ptr<T>(ptr));
  }

  bool findPtr(T* ptr)
  {
    auto it = s.find(ptr);
    if(it == s.end())
      return false;
    return true;
  }

private:
  struct MyCompare;
  std::set<std::unique_ptr<T>, MyCompare> s;

  struct MyCompare
  {
    using is_transparent = void;

    bool operator()(const std::unique_ptr<T>& lhs, const std::unique_ptr<T>& rhs) const
    {
      return lhs < rhs;
    }

    bool operator()(const std::unique_ptr<T>& lhs, const T* rhs) const
    {
      return std::less<const T*>()(lhs.get(), rhs);
    }

    bool operator()(const T* lhs, const std::unique_ptr<T>& rhs) const
    {
      return std::less<const T*>()(lhs, rhs.get());
    }
  };
};

struct B
{
  explicit B(int a_) : a(a_){}
  int a;
};

int main()
{
  A<B> a;

  auto b1 = new B(1);
  auto b2 = new B(2);
  auto b3 = new B(3);

  a.insertPtr(b1);
  a.insertPtr(b2);
  a.insertPtr(b3);

  std::cout << a.findPtr(b3) << std::endl;
}

