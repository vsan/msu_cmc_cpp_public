#include <string>
#include <cassert>
#include <memory>
#include <vector>
#include <iostream>
#include "benchmark/benchmark.h"


template <typename T>
class UniquePtr
{
private:
  T* data;

public:
  UniquePtr(const UniquePtr&) = delete;
  UniquePtr& operator = (const UniquePtr&) = delete;

  UniquePtr(): data(nullptr) {}
  UniquePtr(std::nullptr_t): data(nullptr) {}
  UniquePtr(T * ptr): data(ptr) {}
  UniquePtr(UniquePtr&& other) noexcept : data(other.Release()) {}
  UniquePtr& operator = (std::nullptr_t);
  UniquePtr& operator = (UniquePtr&& other) noexcept;
  ~UniquePtr() { Reset(); }

  T& operator * () const { return *data; }

  T * operator -> () const { return data; }

  T * Release();

  void Reset(std::nullptr_t = nullptr);
  void Reset(T * ptr);

  void Swap(UniquePtr& other);

  T * Get() const;
};

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator = (std::nullptr_t)
{
  Reset();
  return *this;
}

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator = (UniquePtr&& other) noexcept
{
  UniquePtr<T>(other.Release()).Swap(*this);
  return *this;
}

template <typename T>
T * UniquePtr<T>::Release()
{
  T * tmp = data;
  data = nullptr;
  return tmp;
}

template <typename T>
void UniquePtr<T>::Reset(std::nullptr_t)
{
  T * tmp = data;
  data = nullptr;
  if (tmp) {
    delete tmp;
  }
}

template <typename T>
void UniquePtr<T>::Reset(T * ptr)
{
  T * tmp = data;
  data = ptr;
  if (tmp) {
    delete tmp;
  }
}

template <typename T>
void UniquePtr<T>::Swap(UniquePtr& other)
{
  swap(data, other.data);
}

template <typename T>
T * UniquePtr<T>::Get() const
{
  return data;
}

template <typename T, typename ... Args>
UniquePtr<T> MakeUnique(Args&& ... args)
{
  UniquePtr<T> res(new T(std::forward<Args>(args)...));
  return res;
}


void myuniqueptr(benchmark::State& state)
{

  for(auto _ : state)
  {
    UniquePtr<int> p(new int(0));
    benchmark::DoNotOptimize(p);
  }

  state.SetItemsProcessed(state.iterations());
}

BENCHMARK(myuniqueptr);


void rawptr(benchmark::State& state)
{

  for(auto _ : state)
  {
    int *p = new int(0);
    delete p;
    benchmark::DoNotOptimize(p);
  }

  state.SetItemsProcessed(state.iterations());
}

BENCHMARK(rawptr);


void uniqueptr(benchmark::State& state)
{
  for(auto _ : state)
  {
    std::unique_ptr<int> p(new int(0));
    benchmark::DoNotOptimize(p);
  }
  state.SetItemsProcessed(state.iterations());
}
BENCHMARK(uniqueptr);

void sharedptr(benchmark::State& state)
{
  for(auto _ : state)
  {
    std::shared_ptr<int> p(new int(0));
    benchmark::DoNotOptimize(p);
  }
  state.SetItemsProcessed(state.iterations());
}
BENCHMARK(sharedptr);

BENCHMARK_MAIN();