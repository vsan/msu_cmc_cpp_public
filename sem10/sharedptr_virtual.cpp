#include <string>
#include <cassert>
#include <memory>
#include <vector>
#include <iostream>


struct Base
{
  Base()
  {
    std::cout << "Base()\n";
  }
  ~Base()
  {
    std::cout << "~Base()\n";
  }

  virtual void foo() {}
};

struct Derived1 : public Base
{
  Derived1()
  {
    std::cout << "Derived1()\n";
  }
  ~Derived1()
  {
    std::cout << "~Derived1()\n";
  }

  void foo() override {}
};

struct Derived2 : public Base
{
  Derived2()
  {
    std::cout << "Derived2()\n";
  }
  ~Derived2()
  {
    std::cout << "~Derived2()\n";
  }

  void foo() override {}
};

int main()
{
//  std::vector<Base*> obj;
//  for(int i =0; i < 10; ++i)
//  {
//    if(i % 2)
//      obj.push_back(new Derived1);
//    else
//      obj.push_back(new Derived2);
//  }
//
//  for(int i =0; i < 10; ++i)
//    delete obj[i];

//  std::vector<std::unique_ptr<Base>> obj;
//  for(int i =0; i < 10; ++i)
//  {
//    if(i % 2)
//      obj.push_back(std::make_unique<Derived1>());
//    else
//      obj.push_back(std::make_unique<Derived2>());
//  }

// type erasure
  std::vector<std::shared_ptr<Base>> obj;
  for(int i =0; i < 10; ++i)
  {
    if(i % 2)
      obj.push_back(std::make_shared<Derived1>());
    else
      obj.push_back(std::make_shared<Derived2>());
  }
}