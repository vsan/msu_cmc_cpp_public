#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <cmath>
#include "Image.h"

struct int2
{
    int x;
    int y;
};

int2 operator+(const int2& lhs, const int2& rhs)
{
    return {lhs.x + rhs.x, lhs.y + rhs.y};
}

// Liskov substitution principle
class Figure
{
public:
    virtual std::string Name() = 0;
    virtual void Draw(Image& img, int frame) = 0;
    virtual ~Figure() = default;
    virtual std::unique_ptr<Figure> Clone() const = 0;
    virtual void Translate(int2 vec) = 0;
};

class Animator
{
public:
    explicit Animator(int a_amp = 30, double a_delta = 0.1) : amplitude(a_amp), delta(a_delta) {}
    int2 NextFrame()
    {
        double tmp = time;
        time += delta;
        frame++;

        return {int(amplitude * sin(tmp)), int(amplitude * cos(tmp))};
    }
    int GetFrame() const
    {
        return frame;
    }

private:
    int amplitude = 10;
    int frame = 0;
    double time = 0;
    double delta = 0.1;
};

class Triangle : public Figure, Animator
{
    int2 a, b, c;
    Pixel color;
public:
    Triangle(int2  a_a, int2 a_b, int2 a_c, Pixel a_color) : a(a_a), b(a_b), c(a_c), color(a_color) {};
    std::string Name() override
    {
        return "TRIANGLE";
    }

    void Translate(int2 vec) override
    {
      a = a + vec;
      b = b + vec;
      c = c + vec;
    }

    std::unique_ptr<Figure> Clone() const override
    {
      return std::make_unique<Triangle>(a, b, c, color);
    }

    void Draw(Image& img, int frame) override
    {
        while(GetFrame() < frame)
        {
            int2 minP = getMin();
            int2 maxP = getMax();
            for (int i = std::max(minP.x, 0); i < std::min(maxP.x, img.Width()); ++i)
                for (int j = std::max(minP.y, 0); j < std::min(maxP.y, img.Height()); ++j) {
                    int2 p{i, j};
                    bool inside = true;
                    inside &= edgeFunction(a, b, p);
                    inside &= edgeFunction(b, c, p);
                    inside &= edgeFunction(c, a, p);

                    if (inside)
                        img.PutPixel(i, j, color);
                }

            auto delta = NextFrame();
            a = a + delta;
            b = b + delta;
            c = c + delta;
        }
    }
private:
    bool  edgeFunction(int2 A, int2 B, int2 P) const
    {
        return ((P.x - A.x) * (B.y - A.y) - (P.y - A.y) * (B.x - A.x) >= 0);
    }
    int2 getMin() const
    {
        return {std::min(c.x, std::min(a.x, b.x)), std::min(c.y, std::min(a.y, b.y))};
    };
    int2 getMax() const
    {
        return {std::max(c.x, std::max(a.x, b.x)), std::max(c.y, std::max(a.y, b.y))};
    };
};

class Rectangle : public Figure, Animator
{
    int2 a, b;
    Pixel color;
public:
    Rectangle(int2 a_a, int2 a_b, Pixel a_color) : Animator(60, 0.25), a(a_a), b(a_b), color(a_color) {};
    std::string Name() override
    {
        return "RECTANGLE";
    }

    std::unique_ptr<Figure> Clone() const override
    {
      return std::make_unique<Rectangle>(a, b, color);
    }

    void Translate(int2 vec) override
    {
      a = a + vec;
      b = b + vec;
    }

    void Draw(Image& img, int frame) override
    {
        while(GetFrame() < frame)
        {
            for(int i = std::max(a.x, 0); i < std::min(b.x, img.Width()); ++i)
                for(int j = std::max(a.y, 0); j < std::min(b.y, img.Height()); ++j)
                {
                    img.PutPixel(i, j, color);
                }
            auto delta = NextFrame();
            a = a + delta;
            b = b + delta;
        }
    }

};

class Circle : public Figure, Animator
{
    int r;
    int2 o;
    Pixel color;
    int f(int i, int j) const
    {
        return pow((i - o.x), 2) + pow((j - o.y),2) - pow(r,2) < 0;
    }

    void drawCurrent(Image &img)
    {
        for(int i = std::max(o.x - r, 0); i < std::min(o.x + r, img.Width()); ++i)
            for(int j = std::max(o.y - r, 0); j < std::min(o.y + r, img.Height()); ++j)
            {
                if(f(i, j))
                {
                    img.PutPixel(i, j, color);
                }
            }
    }

public:
    Circle(int a_r, int2 a_o, Pixel a_color) : r(a_r), o(a_o), color(a_color) {};
    std::string Name() override
    {
        return "CIRCLE";
    }

    std::unique_ptr<Figure> Clone() const override
    {
      return std::make_unique<Circle>(r, o, color);
    }

    void Translate(int2 vec) override
    {
      o = o + vec;
    }

    void Draw(Image& img, int frame) override
    {
        while(GetFrame() < frame)
        {
            drawCurrent(img);
            o = o + NextFrame();
        }
    }

};


int main()
{
    constexpr int nFrames = 16;
    std::vector<std::unique_ptr<Figure>> figures;

    Image img = Image(512, 512 , 4);

    auto circle = std::make_unique<Circle>(Circle(10, {256, 256}, {255, 0, 0, 255}));
    auto rect = std::make_unique<Rectangle>(Rectangle({0, 0}, {48, 32}, {255, 255, 0, 255}));
    auto tri = std::make_unique<Triangle>(Triangle({100, 300}, {30, 256}, {30, 300}, {0, 255, 0, 255}));
    figures.push_back(std::move(circle));
    figures.push_back(std::move(rect));
    figures.push_back(std::move(tri));

    auto f = figures.back()->Clone();
    f->Translate({100, 0});

    figures.push_back(std::move(f));

    for(auto& fig : figures)
    {
        fig->Draw(img, nFrames);
    }

    img.Save("img.png");
}

