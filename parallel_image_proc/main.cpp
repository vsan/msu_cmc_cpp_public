#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>
#include <vector>
#include <chrono>
#include <sstream>
#include <cmath>
#include <thread>
#include "Image.h"


void imgSum1(const std::string& path1, const std::string& path2)
{
  Image img1(path1);
  Image img2(path2);
  auto w = std::max(img1.Width(), img2.Width());
  auto h = std::max(img1.Height(), img2.Height());
  Image img3(w, h, std::min(img1.Channels(), img2.Channels()));

  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

  for(int y = 0; y < h; ++y)
  {
    for(int x = 0; x < w; ++x)
    {
      auto pix1 = img1.GetPixel(x, y);
      auto pix2 = img2.GetPixel(x, y);
      img3.PutPixel(x, y, pix1 + pix2);
    }
  }


  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "(basic) img1 + img2 duration = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms" << std::endl;

  img3.Save("imgSum1.png");
}

void imgSum2(const std::string& path1, const std::string& path2)
{
  Image img1(path1);
  Image img2(path2);
  auto w = std::max(img1.Width(), img2.Width());
  auto h = std::max(img1.Height(), img2.Height());
  Image img3(w, h, std::min(img1.Channels(), img2.Channels()));

  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

  #pragma omp parallel for
  for(int y = 0; y < h; ++y)
  {
    for(int x = 0; x < w; ++x)
    {
      auto pix1 = img1.GetPixel(x, y);
      auto pix2 = img2.GetPixel(x, y);
      img3.PutPixel(x, y, pix1 + pix2);
    }
  }

  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "(openmp) img1 + img2 duration = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms" << std::endl;

  img3.Save("imgSum2.png");
}

void imgSum2_2(const std::string& path1, const std::string& path2)
{
  Image img1(path1);
  Image img2(path2);
  auto w = std::max(img1.Width(), img2.Width());
  auto h = std::max(img1.Height(), img2.Height());
  Image img3(w, h, std::min(img1.Channels(), img2.Channels()));

  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

#pragma omp parallel for schedule(dynamic) default(none) shared(img1, img2, img3, w, h)
  for(int y = 0; y < h; ++y)
  {
    for(int x = 0; x < w; ++x)
    {
      auto pix1 = img1.GetPixel(x, y);
      auto pix2 = img2.GetPixel(x, y);
      img3.PutPixel(x, y, pix1 + pix2);
    }
  }

  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "(openmp v2) img1 + img2 duration = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms" << std::endl;

  img3.Save("imgSum2_2.png");
}

void imgSum3(const std::string& path1, const std::string& path2)
{
  Image img1(path1);
  Image img2(path2);
  auto w = std::max(img1.Width(), img2.Width());
  auto h = std::max(img1.Height(), img2.Height());
  Image img3(w, h, std::min(img1.Channels(), img2.Channels()));

  const uint32_t min_per_thread = 32;
  const uint32_t max_threads = (w * h + min_per_thread - 1) / min_per_thread;
  const uint32_t hardware_threads = std::thread::hardware_concurrency();
  const uint32_t num_threads = std::min(hardware_threads != 0 ? hardware_threads : 2, max_threads);
  const uint32_t block_size = w * h / num_threads;

  std::cout << "hardware threads " << hardware_threads << "\n"
            << "num threads " << num_threads << "\n"
            << "block_size " << block_size << "\n"
            << "image size " << w * h << "\n";

  std::vector<std::thread> threads(num_threads - 1);
  auto thread_func = [](Image& img1, Image& img2, Image& img3, uint32_t start, uint32_t finish){
    for(auto idx = start; idx < finish; ++idx)
    {
      auto pix1 = img1.GetPixel(idx);
      auto pix2 = img2.GetPixel(idx);
      img3.PutPixel(idx, pix1 + pix2);
    }
  };

  uint32_t start_idx = 0;
  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

  for(int i = 0; i < num_threads - 1; ++i)
  {
    uint32_t finish_idx = start_idx + block_size;
    threads[i] = std::thread(thread_func, std::ref(img1), std::ref(img2), std::ref(img3),
                             start_idx, finish_idx);
    start_idx = finish_idx;
  }

  thread_func(img1, img2, img3, start_idx, w * h);

  for(auto &t : threads)
    t.join();

  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "(std::thread) img1 + img2 duration = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms" << std::endl;

  img3.Save("imgSum3.png");
}

int main()
{
  imgSum1("img1.png", "img2.png");
  imgSum2("img1.png", "img2.png");
  imgSum2_2("img1.png", "img2.png");
  imgSum3("img1.png", "img2.png");
}

