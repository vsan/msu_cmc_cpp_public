#include <iostream>
#include <string>


//isocpp core guidelines - Don't call virtual functions in constructors and destructors

class Base
{
public:

  Base()
  {
    std::cout << "Base()\n";
    f();
  };

  virtual void f()
  {
    std::cout << "Base::f()\n";
  }

  virtual ~Base() = default;
};

class Derived : public Base
{
public:
  Derived()
  {
    std::cout << "Derived()\n";
    f();
  };

  void f() override
  {
    std::cout << "Derived::f()\n";
  }
};


int main()
{
//  Base b;
  Derived d;
}
