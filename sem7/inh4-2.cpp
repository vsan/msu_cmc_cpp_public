#include <iostream>

class Empty
{
public:
  virtual void do_work(){}
};

class Derived : private Empty
{
  int i;
};

class Composed
{
  int i;
  Empty e;
};

struct Derived2 : Empty
{
  Empty c;
  int i;
};

struct Derived3 : Empty
{
  Derived c;
  int i;
};

int main()
{
  std::cout << "sizeof(Empty) = "    << sizeof(Empty) << "\n";
  std::cout << "sizeof(Derived) = "  << sizeof(Derived) << "\n";
  std::cout << "sizeof(Composed) = " << sizeof(Composed) << "\n";

  std::cout << "sizeof(Derived2) = " << sizeof(Derived2) << "\n";
  std::cout << "sizeof(Derived3) = " << sizeof(Derived3) << "\n";
}