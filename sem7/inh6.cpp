//https://en.cppreference.com/w/cpp/language/access


struct Base {
protected:
  int i;
private:
  void g(Base& b, struct Derived& d);
};

struct Derived : Base {
  void f(Base& b, Derived& d) 
  { 
    ++d.i;                      
    ++i;                        
    ++b.i;                      
  }
};

void Base::g(Base& b, Derived& d) 
{ 
  ++i;                              
  ++b.i;                            
  ++d.i;                            
}

void x(Base& b, Derived& d) 
{ 
  ++b.i;                      
  ++d.i;                      
}
int main()
{

}
