#include <iostream>
#include <string>

class INotifier
{
public:
  virtual void Notify(const std::string& message) = 0;
};

class SmsNotifier : public INotifier
{
  std::string m_number;
public:
  SmsNotifier(const std::string& number) : m_number(number) {};
  void Notify(const std::string& message) override
  {
    ////
    std::cout << "SmsNotifier::Notify\n";
  }
};

class EmailNotifier : public INotifier
{
  std::string m_mail;
public:
  EmailNotifier(const std::string& a_mail) : m_mail(a_mail) {};
  void Notify(const std::string& message) override
  {
    ////
    std::cout << "EmailNotifier::Notify\n";
  }
};

void Notify(INotifier& notifier, const std::string& msg)
{
  notifier.Notify(msg);
}


int main()
{
  SmsNotifier sms{"+7111111111"};
  EmailNotifier email{"mail@mail.mail"};

  Notify(sms, "hello");
  Notify(email, "hello");
}
