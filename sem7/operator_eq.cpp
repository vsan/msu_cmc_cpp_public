#include <iostream>
#include <string>

class Base
{
  double x, y;
public:
  void do_work() const
  {
    std::cout << x << ", " << y << "\n";
  }
  Base(): x{0}, y{0}{};

  Base& operator=(double g)
  {
    x = g;
    y = g;
    return *this;
  }

  virtual ~Base() = default;
};

class Derived : public Base
{
public:
  Derived() : Base() {};
  ~Derived() override = default;
//Derived& Derived::operator=(const Derived&)
  Derived& operator=(double g)
  {
    Base::operator=(g);

    return *this;
  }
};


int main()
{
  Base b;
  Derived d;
  b.do_work();
  b = 10.0;
  b.do_work();

  d = 10.0;
  d.do_work();
}
