#include <iostream>
#include <string>

class Base
{
public:
  virtual void do_work()
  {

  }
protected:
  virtual ~Base()
  {
    std::cout << "~Base()\n";
  }
};

class Derived : public Base
{
public:
  void do_work() override
  {

  }
  ~Derived() override
  {
    std::cout << "~Derived()\n";
  }
};


int main()
{
  Base *b = new Derived;
//  delete b;
  auto d = dynamic_cast<Derived*>(b);
  delete d;
}
