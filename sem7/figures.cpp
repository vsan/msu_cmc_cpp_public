#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <cmath>


class Figure
{
public:
  virtual std::string Name() = 0;
  virtual double Area() = 0;
  virtual double Perimeter() = 0;
};


class Triangle : public Figure
{
  double a, b, c;
public:
  Triangle(double  a_a, double a_b, double a_c) : a(a_a), b(a_b), c(a_c) {};
  std::string Name() override
  {
    return "TRIANGLE";
  }

  double Area() override
  {
    double p = 0.5 * Perimeter();
    return sqrt( p * (p - a) * (p - b) * (p - c));
  }

  double Perimeter() override
  {
    return a + b + c;
  }
};

class Rectangle : public Figure
{
  double a, b;
public:
  Rectangle(double  a_a, double a_b) : a(a_a), b(a_b) {};
  std::string Name() override
  {
    return "RECTANGLE";
  }

  double Area() override
  {
    return a * b;
  }

  double Perimeter() override
  {
    return 2.0 * a + 2.0 * b;
  }
};

class Circle : public Figure
{
  double r;
public:
  Circle(double a_r) : r(a_r) {};
  std::string Name() override
  {
    return "CIRCLE";
  }

  double Area() override
  {
    return M_PI * r * r;
  }

  double Perimeter() override
  {
    return 2.0 * M_PI * r;
  }
};

std::shared_ptr<Figure> CreateFigure(std::istream &is)
{
  std::string type;
  is >> type;
  if(type == "RECTANGLE")
  {
    double a, b;
    is >> a >> b;
    return std::make_shared<Rectangle>(a, b);
  }
  else if(type == "TRIANGLE")
  {
    double a, b, c;
    is >> a >> b >> c;
    return std::make_shared<Triangle>(a, b, c);
  }
  else if(type == "CIRCLE")
  {
    double r;
    is >> r;
    return std::make_shared<Circle>(r);
  }
  return nullptr;
}

int main()
{
  std::vector<std::shared_ptr<Figure>> figure;

  for(std::string line; std::getline(std::cin, line);)
  {
    std::istringstream is(line);

    figure.push_back(CreateFigure(is));
  }

  for(auto& p : figure)
  {
    std::cout << p->Name() << " " << p->Perimeter() << " " << p->Area() << "\n";
  }
}

