#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>

struct Base
{
  int a = 0;
  int b = 0;
  int c = 0;

  Base()
  {
    std::cout << "Base() : ";
    std::cout << "a = " << a << " b = " << b << " c = " << c << "\n";
  }
};
// every object of type Derived includes Base as a subobject

struct Derived : Base
{
  int b = 1;

  Derived()
  {
    std::cout << "Derived() : ";
    std::cout << "a = " << a << " b = " << b << " c = " << c << " ";
    std::cout << "Base::b = " << ::Base::b << "\n";
  }
};

// every object of type Derived2 includes Derived and Base as subobjects
struct Derived2 : Derived
{
  int c = 2;
  int b = 2;

  Derived2()
  {
    std::cout << "Derived2() : ";
    std::cout << "a = " << a << " b = " << b << " c = " << c << " ";
    std::cout << " Base::b = " << ::Base::b << " Derived::b = " << ::Derived::b << " Base::c = " << ::Base::c << "\n";
  }
};

int main()
{
//  Base b;
//  Derived d;
  Derived2 d2;
  // как вы думаете, что будет выведено и почему?
  std::cout << "sizeof(Base) = " << sizeof(Base) << "\n";
  std::cout << "sizeof(Derived) = " << sizeof(Derived) << "\n";
  std::cout << "sizeof(Derived2) = " << sizeof(Derived2) << "\n";
}