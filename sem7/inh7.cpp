#include <iostream>

class Animal
{
  int arr[10];
public:
  Animal()
  {
    std::cout << "Animal()\n";
  }
  virtual void f(){};
};

class Bird : Animal
{
public:
  Bird()
  {
    std::cout << "Bird()\n";
  }
};

class FlyingAnimal : Animal
{
public:
  FlyingAnimal()
  {
    std::cout << "FlyingAnimal()\n";
  }
  virtual void fly()
  {
    std::cout << "FlyingAnimal::fly()\n";
  }
};

class Eagle : public Bird, public FlyingAnimal
{
  int x = 0;
public:
  Eagle()
  {
    std::cout << "Eagle()\n";
  }

  void fly() override
  {
    std::cout << "Eagle::fly()\n";
  }
};

void foo(FlyingAnimal *f)
{
  f->fly();
}

void foo(FlyingAnimal f) //срезка
{
  f.fly();
}

int main()
{
  Eagle *e = new Eagle;
  std::cout << "sizeof(Eagle) = " << sizeof(Eagle) << "\n";

  Bird *b = e;
  FlyingAnimal *f = e;

  Bird *b2 = new Eagle;
  FlyingAnimal *f2 = dynamic_cast<FlyingAnimal*>(b2);

//  e->fly();
//  f->fly();
//  f2->fly();

  foo(f);
  foo(*f);

  FlyingAnimal f3 = *e;
}
