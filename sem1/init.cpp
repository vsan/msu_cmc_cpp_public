#include <iostream>
#include <vector>

auto func(auto a, auto b)
{
  return a + b;
}

int main()
{
  int a1 = 7;
  int a2(1);
  int a3{7};
  int a3_ = {1};
  int a4{};
  auto a5 = int();
//  int a6(); //function declaration

//  std::cout << a1;
//
//  std::vector<int> vec = {1, 2, 3};
//
//  for(const auto& el: vec)
//  {
//    std::cout << el << "\n";
//  }

  auto b = 2.6;
  auto c = b;
  std::cout << func(b, c) << "\n";
}