#include <iostream>
#include <iomanip>
#include <vector>


int main()
{
  int sum{0};

  //0, 1, 2
  //std::cin, std::cout, std::cerr
//  int a;
//  std::cout << "read numbers\n";
//  std::cerr << "read numbers\n";
//  while(std::cin >> a)
////  while(!(std::cin >> a).eof())
//  {
//    sum += a;
//  }

//  while(1)
//  {
//    int a;
//    std::cin >> a;
//    if(std::cin.eof())
//      break;
//
//    sum += a;
//  }
//
//  std::cout << std::hex << std::setw(10) << std::setfill('0') << sum
//            << " " << std::dec << sum << "\n";

// Lines
  std::string line;
  while(getline(std::cin, line))
  {
    std::cout << "line : " << line;
  }

//  char c;
//  while(std::cin.get(c))
//  {
//    std::cout << "[" << c << "]" << "\n";
//  }

//  std::cin >> std::noskipws;
//  int a;
//  while(std::cin >> a)
//  {
//    std::cout << "[" << a << "]" << "\n";
//  }
//  std::cin.clear();
//  std::cin.ignore();
//  std::cin >> std::skipws;
//  while(std::cin >> c)
//  {
//    std::cout << "hello" << "\n";
//    std::cout << "[" << c << "]" << "\n";
//  }
}