#include <iostream>


class Point2D
{
public:
  void negate()
  {
    x = -x;
    y = -y;
  }

  void print()
  {
    std::cout << x << " " << y << "\n";
  }

private:
  int x = 5;
  int y = 10;
};


int main()
{
  Point2D pt;
  pt.print();
  pt.negate();
  pt.print();
}