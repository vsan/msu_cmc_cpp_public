#include <cstdio>
#include <string>
#include <iostream>

void print(char* str)
{
  printf("%s\n", str);
}


int main()
{
  int a = 10;
  float b = reinterpret_cast<float&>(a);

  std::cout << "a = " << a << " b = " << b << "\n";

  std::string str = "Hello";

  print(const_cast<char*>(str.c_str()));

  
// static_cast
// dynamic_cast
// const_cast
// reinterpret_cast

  return 0;
}
