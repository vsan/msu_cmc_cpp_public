#include <iostream>
#include <cstdio>

namespace A
{
  namespace B
  {
    int myfunc(int a, int b)
    {
      return a + b;
    }

    int myfunc(int a, int b, int c)
    {
      return a + b + c;
    }

    float myfunc(float a, float b)
    {
      return a + b;
    }
  }
}


//int myfunc(int a, int b)
//{
//  return a * b;
//}

struct Point2D
{
  int x;
  int y;
};

std::ostream& operator<<(std::ostream& stream, const Point2D &pt)
{
  stream << pt.x << " " << pt.y <<"\n";

  return stream;
}

int a = 16;

namespace
{
  int q = 1;
  int w = 1;
}


int main()
{
  using std::cout;
  int a = 1;
  int b = 2;

  Point2D pt{a, b};

  std::cout << "point :" << pt << "\n";

  std::cout << "myfunc(a, b) = " << A::B::myfunc(::a, ::q) <<
               "; A::B::myfunc(a, b) = " << /*A::B::myfunc(a, b)*/A::B::myfunc(a, ::w, 1) << "\n";
}