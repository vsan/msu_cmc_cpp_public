#include <iostream>
#include <vector>


struct Point2D
{
  int x;
  int y;
};

std::ostream& operator<<(std::ostream& stream, const Point2D &pt)
{
  stream << pt.x << " " << pt.y <<"\n";

  return stream;
}

void swap(int &a, int &b)
{
  int x = a;
  a = b;
  b = x;
}

void print_arr(const std::vector<int> &arr)
{
  for(const int& elem : arr)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}


int& imax(int *arr, size_t sz)
{
  int max = std::numeric_limits<int>::min();
  int maxind = -1;
  for(int i = 0; i < sz; ++i)
  {
    if(arr[i] > max)
    {
      max = arr[i];
      maxind = i;
    }
  }

  return arr[maxind];
}

int main()
{
  int aaa = 1;
  int bbb = 2;
  Point2D pt{aaa, bbb};

  std::cout << "point :" << pt << "\n";

  int a = 10;
  int &b = a;
  int *ptr = &a;

  b = aaa;
  b = 11;
  b++;

  int &c{b};
  int d = 200;

  swap(c, d);
  std::cout << c << " " << d << "\n";

  std::vector<int> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  print_arr(arr);

  int arr2[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  int& maxel = imax(arr2, 10);
  maxel *= -1;
  std::cout << maxel << "\n";
}