#include <iostream>

struct A
{
  int x;
  int y;
};

class SimpleVectorInt
{
public:
  SimpleVectorInt() : data(nullptr), size(0), capacity(0)
  {
    std::cout <<"Default ctor\n";
  }

//  SimpleVectorInt(const SimpleVectorInt& other)
//  {
//    std::cout <<"Default ctor\n";
//  }

//  SimpleVectorInt& operator=(const SimpleVectorInt& other)
//  {
//
//  }

  // RAII - resource acquisition is initialization
  explicit SimpleVectorInt(size_t a_size) //
  {
    data = new int[2 * a_size];
    capacity = 2 * a_size;
    size = a_size;
    std::cout <<"Param ctor\n";
  }

  SimpleVectorInt(size_t a_size, int def_val) : SimpleVectorInt(a_size)
  {
    for(int i = 0; i < a_size; ++i)
      data[i] = def_val;

    std::cout <<"Param ctor 2\n";
  }

  ~SimpleVectorInt()
  {
    std::cout <<"dtor\n";
    delete [] data;
  }

  int& operator[](size_t index)
  {
    return data[index];
  }

  const int& operator[](size_t index) const
  {
    return data[index];
  }

  void PushBack(int value)
  {
    if(size == capacity)
    {
      ReAlloc();
    }
    *(data + size) = value;
    size++;
  }

  size_t Size() const
  {
    return size;
  }

  size_t Capacity() const
  {
    return capacity;
  }

private:
  int* data;// = nullptr;
  size_t size;// = 0;
  size_t capacity;// = 0;

  void ReAlloc()
  {
    if(capacity == 0)
    {
      data = new int [1];
      capacity = 1;
      std::cout <<"Realloc 1\n";
    }
    else
    {
      std::cout <<"Realloc 2\n";
      int *new_alloc = new int[capacity * 2];
      std::move(data, data + size, new_alloc);
      capacity *= 2;
      delete [] data;
      data = new_alloc;
    }
  }
};


void printVec(const SimpleVectorInt& vec)
{
  for(int i = 0; i < vec.Size(); ++i)
  {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

// Rule of 0/3/5

int main()
{
  SimpleVectorInt myvec;
  myvec.PushBack(10);
  myvec.PushBack(20);
  myvec.PushBack(30);
  myvec.PushBack(40);

  printVec(myvec);
  myvec[2] *= 2;
  printVec(myvec);

  SimpleVectorInt myvec2(10);
  //SimpleVectorInt myvec3 = 10;
  printVec(myvec2);

  SimpleVectorInt myvec3(10, 100);
  printVec(myvec3);

//  SimpleVectorInt myvec4 = myvec3;
//  SimpleVectorInt myvec5(myvec3);
//
//  printVec(myvec4);
//  printVec(myvec5);
}