#include <iostream>
#include <vector>
#include <exception>
#include <string>

class MyException : public std::exception
{
private:
  std::string error_string;
public:
  explicit MyException(const std::string& msg) : error_string(msg)
  {
    std::cout <<"MyException()\n";
  }

  const char* what() const noexcept override
  {
    return error_string.c_str();
  }

  ~MyException() override
  {
    std::cout <<"~MyException()\n";
  }
};



int main()
{
  try
  {
    std::string("abcdef").substr(20);
    std::vector<int> v = {1, 2, 3, 4, 5};
    std::cout << "v[10] = " << v.at(10) << std::endl;
    throw MyException("oops\n");
  }
  catch (MyException& e)
  {
    std::cout << e.what();
  }
  catch (std::exception &e)
  {
    std::cout << "std::exception: " << e.what() << "\n";
  }
  catch (...)
  {
    std::cout << "some exception\n";
  }

}
