#include <iostream>
#include <vector>
#include <exception>
#include <string>

void foobar()
{
  std::cout << "foobar()\n";
  throw "OOPS";
}

void baz()
{
  std::cout << "baz()\n";
  foobar();
}

void bar()
{
  std::cout << "bar()\n";
  baz();
}

void foo()
{
  std::cout << "foo()\n";
  try
  {
    bar();
  }
  catch(...)
  {
    std::cout << "some exception\n";
  }
}


int main()
{
  foo();

  std::cout << "Exiting...\n";
}
