#include <iostream>
#include <string>


//CRTP = Curiously Recurring Template Pattern
template <typename T>
struct Base
{
  void foo()
  {
    std::cout << "Base::foo()\n";
  }
  void caller()
  {
    (static_cast<T*>(this))->foo();
  }
};

struct Derived : public Base<Derived>
{
  void foo()
  {
    std::cout << "Derived::foo()\n";
  }
};

struct AnotherDerived : public Base<AnotherDerived>
{
  void foo()
  {
    std::cout << "AnotherDerived::foo()\n";
  }
};

struct SomeOtherClass : public Base<SomeOtherClass>
{
};

template<typename T>
void ProcessFoo(Base<T> *b)
{
  b->caller();
}

int main()
{
  Derived d1;
  AnotherDerived d2;
  SomeOtherClass s1;

  ProcessFoo(&d1);
  ProcessFoo(&d2);
  ProcessFoo(&s1);
}
