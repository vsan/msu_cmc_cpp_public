#include <iostream>
#include <vector>
#include <exception>
#include <string>

struct Base
{
  Base()
  {
    std::cout << "Base()\n";
  }
  virtual ~Base()
  {
    std::cout <<"~Base()\n";
  }
};

struct Derived : Base
{
  Derived()
  {
    std::cout << "Derived()\n";
  }
  virtual ~Derived()
  {
    std::cout <<"~Derived()\n";
  }
};

static Derived s_d;

int main()
{
  std::exception_ptr eptr;
  try
  {
//    Derived d;
//    Base b;
    Base *bp = new Derived;
    std::string("abx").substr(10);
    std::cout << "some msg\n";
    for(int i = 0; i != 100; ++i)
    {
      std::cout << i << "\n";
    }
  }
  catch (...)
  {
    std::cout << "some exception\n";
  }

  std::cout << "Exiting...\n";
}
