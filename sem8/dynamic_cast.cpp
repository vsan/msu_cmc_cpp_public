#include <iostream>

class Base
{
public:
  virtual void f()
  {
    std::cout << "Base::f()" << std::endl;
  }
};

class Derived : public Base
{
public:
  void f() override
  {
    std::cout << "Derived::f()" << std::endl;
  }

};

int main()
{
  Base *b1 = new Derived;
  Base *b2 = new Base;

  Derived* d1 = dynamic_cast<Derived*>(b1);
  Derived* d2 = dynamic_cast<Derived*>(b2);
  std::cout << "d1 = " << d1 << "\n";
  std::cout << "d2 = " << d2 << "\n";
}