#include "benchmark/benchmark.h"


// https://github.com/google/benchmark

#define REPEAT2(x) x x
#define REPEAT4(x) REPEAT2(x) REPEAT2(x)
#define REPEAT8(x) REPEAT4(x) REPEAT4(x)
#define REPEAT16(x) REPEAT8(x) REPEAT8(x)
#define REPEAT32(x) REPEAT16(x) REPEAT16(x)
#define REPEAT(x) REPEAT32(x)



class A
{
  int m_i;
public:
  A() : m_i{0} {}
  void f(int i) {m_i += i;}
  int get() const {return m_i;}
};


class Base
{
protected:
  int m_i;
public:
  Base() : m_i{0} {}
  virtual ~Base() {}
  virtual void f(int i) = 0;
  int get() const {return m_i;}
};

class Derived : public Base
{
public:
  void f(int i) { m_i += i;};

};

template <typename D> class B
{
protected:
  int m_i;
public:
  B() : m_i{0} {}
  ~B() {}
  void f(int i) {static_cast<D*>(this)->f(i);};
  int get() const {return m_i;}
};

class D: public B<D>
{
public:
  void f(int i )
  {
    m_i +=i;
  }
};


void not_poly(benchmark::State& state)
{
  A * a = new A;
  int i = 0;

  for(auto _ : state)
  {
    REPEAT(a->f(++i);)
  }

  benchmark::DoNotOptimize(a->get());
  state.SetItemsProcessed(32 * state.iterations());
  delete a;
}

void poly(benchmark::State& state)
{
  Base * a = new Derived;
  int i = 0;

  for(auto _ : state)
  {
    REPEAT(a->f(++i);)
  }

  benchmark::DoNotOptimize(a->get());
  state.SetItemsProcessed(32 * state.iterations());
  delete a;
}

void static_p(benchmark::State& state)
{
  B<D> *b = new D;
  int i = 0;

  for(auto _ : state)
  {
    REPEAT(b->f(++i);)
  }

  benchmark::DoNotOptimize(b->get());
  state.SetItemsProcessed(32 * state.iterations());
  delete b;
}

BENCHMARK(not_poly);
BENCHMARK(poly);
BENCHMARK(static_p);

BENCHMARK_MAIN();