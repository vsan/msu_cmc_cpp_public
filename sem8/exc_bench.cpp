#include <iostream>
#include <stdexcept>
#include <variant>
#include <chrono>

enum class MyError
{
  OutOfBounds, Memory, InputOutput, Timeout, Unknown
};

enum class MyError2
{
  NoError, OutOfBounds, Memory, InputOutput, Timeout, Unknown
};

constexpr uint32_t nErrorTypes = 5;

MyError2 DoWorkErrCode(uint32_t i, uint32_t& res) noexcept
{
  if(i % 20 == 0)
  {
    return static_cast<MyError2>((i / 20) % (nErrorTypes));
  }
  else
  {
    res = i + 1;
    return MyError2::NoError;
  }
}

class MyException : public std::exception
{
private:
  std::string error_string;
public:
  explicit MyException(const std::string& msg) : error_string(msg){}

  const char* what() const noexcept override
  {
    return error_string.c_str();
  }
};

uint32_t DoWorkException(uint32_t i)
{
  if(i % 5'00 == 0)
  {
    throw MyException(std::to_string((i / 20) % nErrorTypes ));
  }
  else
  {
    return i + 1;
  }
}

std::variant<uint32_t, MyError> DoWorkVariant(uint32_t i) noexcept
{
  if(i % 20 == 0)
  {
    return static_cast<MyError>((i / 20) % (nErrorTypes));
  }
  else
  {
    return i + 1;
  }
}

constexpr uint32_t ITERATIONS = 4'000'000;
int main ()
{
  {
    uint32_t error_count = 0;
    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};
    uint32_t res = 0;
    for(uint32_t i = 0; i < ITERATIONS; ++i)
    {
      if(DoWorkErrCode(i, res) != MyError2::NoError)
        error_count++;
    }
    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;
    std::cout << "DoWorkErrCode(): error_count = " << error_count << " ; duration = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms\n";
  }

  {
    uint32_t error_count = 0;
    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};
    for(uint32_t i = 0; i < ITERATIONS; ++i)
    {
      try
      {
        DoWorkException(i);
      }
      catch(MyException& e)
      {
        error_count++;
      }
    }
    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;
    std::cout << "DoWorkException(): error_count = " << error_count << " ; duration = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms\n";
  }

  {
    uint32_t error_count = 0;
    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};
    uint32_t res = 0;
    for(uint32_t i = 0; i < ITERATIONS; ++i)
    {
      if(const auto res = DoWorkVariant(i); std::holds_alternative<MyError>(res))
      {
        error_count++;
      }
    }
    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;
    std::cout << "DoWorkVariant(): error_count = " << error_count << " ; duration = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms\n";
  }
}