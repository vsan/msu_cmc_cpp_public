#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <memory>

struct A{};

A* MakeA()
{
  return new A;
}

int main()
{
  A a;
  {
    A *ptr = new A;
    std::unique_ptr<A> up1(ptr);
//    std::unique_ptr<A> up2(ptr);
  }

  {
    A *ptr = new A;
    std::shared_ptr<A> up1(ptr);
//    std::shared_ptr<A> up2(ptr);
  }
  {
    A *ptr = new A;
    std::shared_ptr<A> up1(ptr);
    std::shared_ptr<A> up2 = up1;
  }

  {
    A *ptr = new A;
    std::unique_ptr<A> up1(ptr);
    std::unique_ptr<A> up2 = std::move(up1);
  }

  {
    std::unique_ptr<A> up1(MakeA());
    std::shared_ptr<A> up2(MakeA());
  }

  {
    auto up1 = std::make_unique<A>();
//    std::unique_ptr<A> up2(up1.get());
  }
}
