#define REPEAT2(x) x x
#define REPEAT4(x) REPEAT2(x) REPEAT2(x)
#define REPEAT8(x) REPEAT4(x) REPEAT4(x)
#define REPEAT16(x) REPEAT8(x) REPEAT8(x)
#define REPEAT32(x) REPEAT16(x) REPEAT16(x)
#define REPEAT(x) REPEAT32(x)


// вариант "полностью статического" синглтона
class SingletonS
{
public:
  static SingletonS& instance()
  {

    return inst;
  }
  int& GetResource() {return resource;}
private:

  SingletonS() : resource{0} {}
  SingletonS(const SingletonS&) = delete;
  SingletonS& operator=(const SingletonS&) = delete;
  ~SingletonS()
  {
    // cleanup
  }

  static SingletonS inst;
  int resource;
};
SingletonS SingletonS::inst;
//////////////////////////////////////////


// Синглтон Мейерса
class SingletonM
{
public:
  static SingletonM& instance()
  {
    static SingletonM inst;
    return inst;
  }
  int& GetResource() {return resource;}
private:
  SingletonM() : resource{0} {}
  SingletonM(const SingletonM&) = delete;
  SingletonM& operator=(const SingletonM&) = delete;
  ~SingletonM()
  {
    // cleanup
  }

  int resource;
};
//////////////////////////////////////////


// Синглтон + pImpl (паттерн pointer to implementation)
// отделяем интерфейс от реализации
// объектов SingletonPimpl может быть сколько угодно,
// но все работают с одной "реализацией"
//Interface (header)
struct SingletonImpl;
class SingletonPimpl
{
public:
  int& GetResource();
private:
  static SingletonImpl& impl();
};

//Implementation (.cpp)
struct SingletonImpl
{
  SingletonImpl(): resource{0} {}
  int resource;
};

int& SingletonPimpl::GetResource()
{
  return impl().resource;
}
SingletonImpl& SingletonPimpl::impl()
{
  static SingletonImpl instance;
  return instance;
}
//////////////////////////////////////////


// Оптимизация - храним ссылку на реализацию, чтобы не проверять
// инициализацию статического объекта каждый раз
// сама реализация осталась такая же как в пред. варианте
class SingletonPimplCachedRef
{
public:
  SingletonPimplCachedRef();
  int& GetResource();
private:
  static SingletonImpl& impl();
  SingletonImpl& m_impl;
};

SingletonPimplCachedRef::SingletonPimplCachedRef() : m_impl(impl()) {}

int& SingletonPimplCachedRef::GetResource()
{
  return m_impl.resource;
}
SingletonImpl& SingletonPimplCachedRef::impl()
{
  static SingletonImpl instance;
  return instance;
}
//////////////////////////////////////////

static void singleton_static(benchmark::State& state)
{
  for (auto _ : state)
  {
    REPEAT(benchmark::DoNotOptimize(++SingletonS::instance().GetResource());)
  }
  state.SetItemsProcessed(32 * state.iterations());
}
BENCHMARK(singleton_static);

static void singleton_meyers(benchmark::State& state)
{
  for (auto _ : state)
  {
    REPEAT(benchmark::DoNotOptimize(++SingletonM::instance().GetResource());)
  }
  state.SetItemsProcessed(32 * state.iterations());
}
BENCHMARK(singleton_meyers);

static void singleton_meyers_cache_ref(benchmark::State& state)
{
  SingletonM& s_ref = SingletonM::instance();
  for (auto _ : state)
  {
    REPEAT(benchmark::DoNotOptimize(++s_ref.GetResource());)
  }
  state.SetItemsProcessed(32 * state.iterations());
}
BENCHMARK(singleton_meyers_cache_ref);

static void singleton_pimpl(benchmark::State& state)
{
  SingletonPimpl s;
  for (auto _ : state)
  {
    REPEAT(benchmark::DoNotOptimize(++s.GetResource());)
  }
  state.SetItemsProcessed(32 * state.iterations());
}
BENCHMARK(singleton_pimpl);

static void singleton_pimpl_cache(benchmark::State& state)
{
  SingletonPimplCachedRef s;
  for (auto _ : state)
  {
    REPEAT(benchmark::DoNotOptimize(++s.GetResource());)
  }
  state.SetItemsProcessed(32 * state.iterations());
}
BENCHMARK(singleton_pimpl_cache);