#include <iostream>

struct Base
{
  virtual void foo() final {}
};

struct Derived1 final : Base
{
};

struct Derived2 : Derived1
{
};

struct Derived3 : Base
{
  void foo() override {}
};

int main()
{

}