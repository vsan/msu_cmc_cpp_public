#include <iostream>
#include <utility>
#include <vector>
#include <string>


void someCtr(std::string& v)
{
  std::cout << "lvalue reference" << std::endl;
}

void someCtr(const std::string& v)
{
  std::cout << "const lvalue reference" << std::endl;
}

void someCtr(std::string&& v)
{
  std::cout << "rvalue reference" << std::endl;
}

//template <typename T>
//void factory(T v)
//{
//  someCtr(v);
//}

//template <typename T>
//void factory(const T& v)
//{
//  someCtr(v);
//}

template <typename T>
void factory(T&& v)
{
  std::cout<<"\tnormal: ";
  someCtr(v);
  std::cout<<"\tstd::forward: ";
  someCtr(std::forward<T>(v));
  std::cout<<"\tstatic_cast<T&&>: ";
  someCtr(static_cast<T&&>(v));
  std::cout<<"\tstd::move: ";
  someCtr(std::move(v));
}

//collapsing references
/*
 * Function param type   Argument type    Deduced type
 * T&                    lvalue ref       T&
 * T&                    rvalue ref       T&
 * T&&                   lvalue ref       T&
 * T&&                   rvalue ref       T&&
 */

int main()
{
  std::string a = "a";
  const std::string b = "b";

  std::cout << "string\n";
  factory(a);
  std::cout << "const string\n";
  factory(b);

  std::string c = "c";
  std::cout << "temp string\n";
  factory(c + "!");

  std::cout << "\n";
}
