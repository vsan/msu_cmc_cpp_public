#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <type_traits>


template <typename T>
struct A
{
  typedef T value_type;

  T data = 0;
};


void print(std::string& msg)
{
  std::cout << "msg: " << msg << std::endl;
}

int main()
{
  std::vector<A<int>::value_type> v = {1, 2, 3, 4, 5};

  for(auto& e : v)
  {
    std::cout << e << " ";
  }

  std::cout << "\n";
}