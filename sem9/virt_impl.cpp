struct foo {
  virtual void bar() = 0;
};

void foo::bar() { /* default implementation */ }

class foof : public foo {
  void bar() { foo::bar(); } // have to explicitly call default implementation.
};