#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <type_traits>

// SFINAE - substitution failure is not an error
template <typename Func, typename Container,
    typename = std::enable_if_t<std::is_invocable_v<Func&, typename Container::value_type&>> >
void Apply(Func f, Container& c)
{
  for(auto& elem: c)
  {
    f(elem);
  }
}


void print(std::string& msg)
{
  std::cout << "msg: " << msg << std::endl;
}

int main()
{
  std::vector v = {1, 2, 3, 4, 5};

  Apply([](int& a){ a+= 10;}, v);
//  Apply([](int* a){ a++;}, v);
//  Apply(print, v);

  for(auto& e : v)
  {
    std::cout << e << " ";
  }

  std::cout << "\n";
}