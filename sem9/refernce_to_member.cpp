#include <iostream>

struct A
{
  void foo() &
  {
    std::cout << "foo() &\n";
  }

  void foo() const &
  {
    std::cout << "foo() const &\n";
  }

  void foo() &&
  {
    std::cout << "foo() &&\n";
  }

//  void foo() && = delete;
};

int main()
{
  A a1;
  a1.foo();

  const A a2;
  a2.foo();

  A().foo();
}