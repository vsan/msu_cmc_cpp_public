#include <iostream>
#include <utility>
#include <vector>
#include <string>

template <typename D>
struct not_equal
{
  bool operator!=(const D& rhs) const
  {
    return !static_cast<const D*>(this)->operator==(rhs);
  }
};

class C : public not_equal<C>
{
  int i_;
public:
  explicit C(int i) : i_(i){}
  bool operator==(const C& rhs) const
  {
    return i_ == rhs.i_;
  }
};

class C2 : public not_equal<C2>
{
  int x_;
  int y_;
public:
  explicit C2(int x, int y) : x_(x), y_(y){}
  bool operator==(const C2& rhs) const
  {
    return (x_ == rhs.x_) && (y_ == rhs.y_);
  }
};


int main()
{
  C c1(1);
  C c2(2);
  C c3(1);

  std::cout << "c1, c2: " << (c1 == c2) << " " << (c1 != c2) << std::endl;
  std::cout << "c1, c3: " << (c1 == c3) << " " << (c1 != c3) << std::endl;

  C2 cc1(1, 1);
  C2 cc2(1, 2);
  std::cout << "cc1, cc2: " << (cc1 == cc2) << " " << (cc1 != cc2) << std::endl;
}
