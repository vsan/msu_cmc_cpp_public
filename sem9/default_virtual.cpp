#include <iostream>

struct foo
{
  virtual void bar() = 0;
  virtual ~foo() = default;
};

void foo::bar()
{
  std::cout << "default purely virtual\n";
}

struct foof : public foo
{
  void bar() override
  {
    std::cout << "overridden func\n";
    foo::bar();
  }
};

int main()
{
  foof f;
  f.bar();
}