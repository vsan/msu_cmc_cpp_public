#include <stdio.h>
#include <stdlib.h>


void hello()
{
  int *ptr = malloc(sizeof(int));
  *ptr = 999;
  printf("%d; %s\n", *ptr, (sizeof('a') == sizeof(char)) ? "C++" : "C");
  free(ptr);
}
