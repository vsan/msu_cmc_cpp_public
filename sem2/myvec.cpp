#include <iostream>



class SimpleVectorInt
{
public:

  SimpleVectorInt() : data(nullptr), size(0), capacity(0)
  {
    std::cout <<"Default ctor\n";
  }

  SimpleVectorInt(const SimpleVectorInt& other) : data(nullptr), size(0), capacity(0)
  {
    std::cout <<"Copy ctor\n";
    if(this->capacity < other.capacity)
    {
      this->capacity = other.capacity;
      ReAlloc();
    }
    size = other.size;
    for(int i = 0; i < size; ++i)
    {
      this->data[i] = other.data[i];
    }
  }

//  SimpleVectorInt(const SimpleVectorInt&& other)
//  {
//    std::cout <<"Move ctor\n";
//  }
  SimpleVectorInt& operator=(const SimpleVectorInt& other)
  {
    if(this != &other)
    {
      if(this->capacity < other.capacity)
      {
        capacity = other.capacity;
        ReAlloc();
      }
      size = other.size;
      for(int i = 0; i < size; ++i)
      {
        this->data[i] = other.data[i];
      }
    }
    return *this;
  }

  explicit SimpleVectorInt(size_t a_size)
  {
    data = new int[2 * a_size];
    capacity = 2 * a_size;
    size = a_size;
    std::cout <<"Param ctor\n";
  }

  SimpleVectorInt(size_t a_size, int def_val) : SimpleVectorInt(a_size)
  {
    for(int i = 0; i < a_size; ++i)
      data[i] = def_val;

    std::cout <<"Param ctor 2\n";
  }

  ~SimpleVectorInt()
  {
    std::cout <<"dtor\n";
    delete [] data;
  }

  int& operator[](size_t index)
  {
    return data[index];
  }

  const int& operator[](size_t index) const
  {
    return data[index];
  }

  void PushBack(int value)
  {
    if(size == capacity)
    {
      ReAlloc();
    }
    *(data + size) = value;
    size++;
  }

  size_t Size() const
  {
    return size;
  }

  size_t Capacity() const
  {
    return capacity;
  }

private:
  int* data;// = nullptr;
  size_t size;// = 0;
  size_t capacity;// = 0;

  void ReAlloc()
  {
    if(capacity == 0)
    {
      data = new int [1];
      capacity = 1;
      std::cout <<"Realloc 1\n";
    }
    else
    {
      std::cout <<"Realloc 2\n";
      int *new_alloc = new int[capacity * 2];
      if(data != nullptr)
      {
        std::move(data, data + size, new_alloc);
        delete[] data;
      }
      capacity *= 2;
      data = new_alloc;
    }
  }
};

struct A
{
  A() : vec(10, 10)
  {
  }
  SimpleVectorInt vec;
  int a;
};


void printVec(const SimpleVectorInt& vec)
{
  for(int i = 0; i < vec.Size(); ++i)
  {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

// Rule of 0/3/5


int main()
{
  constexpr int bb = 1;
//  A a;
  SimpleVectorInt myvec; //default ctor
  myvec.PushBack(10);
  myvec.PushBack(20);
  myvec.PushBack(30);
  myvec.PushBack(40);

//  printVec(myvec);
  myvec[2] *= 2;
//  printVec(myvec);

//  SimpleVectorInt myvec2(10);
//  printVec(myvec2);
  SimpleVectorInt myvec3(10, 100);
//  printVec(myvec3);
//
//  SimpleVectorInt myvec3_2 = SimpleVectorInt(2, 3); // SimpleVectorInt myvec3_2(2, 3)
//  printVec(myvec3_2);
  SimpleVectorInt myvec4(myvec);
//  printVec(myvec4);

//  if((myvec4 = myvec3).Size() != 0)
//    printVec(myvec4);
  SimpleVectorInt myvec5(myvec3);
//  printVec(myvec4);
//  printVec(myvec5);

  const SimpleVectorInt myvec_c(myvec3);
//  printVec(myvec_c);
}