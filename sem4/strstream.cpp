#include <iostream>
#include <sstream>
#include <iomanip>

int main()
{

  float a = 666.123456789f;
  uint32_t b = 123456u;
  std::stringstream ss;

  ss << "0x" << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << b << "\n";
  ss << "\"woah\" " << std::setw(10) << std::setfill('0') << a << " " << std::dec << a << "\n";
  ss << "hello " << 123456 << " " << 2.5 << "\n";

  std::cout << ss.str();

}