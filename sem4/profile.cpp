#include <chrono>
#include <iostream>
#include <string>
#include <vector>


void doWork()
{
  std::vector<int> v;

  for(int i = 0; i < 1'000; ++i)
  {
    v.insert(v.begin() + (v.size()/2), i);
  }
}

int main()
{
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  doWork();

  auto finish = std::chrono::steady_clock::now();
  auto dur = finish - start;
  std::cout << "Time elapsed: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms"
            << " or " << std::chrono::duration_cast<std::chrono::nanoseconds>(dur).count() << " ns\n";

}