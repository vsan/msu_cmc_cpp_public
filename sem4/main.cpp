#include <chrono>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <algorithm>


using namespace std;
int main()
{
  std::string str = "abc"; //
//  cout << str[1] << endl;

  std::string str2{"abc123"};
  std::string str3(10, 'a');
  std::string str4{"abcdef", 4};
  std::string str5{str2, 2, 1};
  std::string str6{str2, 2};
  std::string str7;

//  cout << str7 << endl;
//  cout << str2 << endl;
//  cout << str3 << endl;
  cout << str4 << endl;
  cout << str5 << endl;
  cout << str6 << endl;

  str7.assign(str2, 1, 4);
  cout << str7 << endl;

  cout << str4.size() << " " << str4.length() << " " << str4.capacity() << std::endl;
  str4 += "abcabcabcaabcabababab";

  cout << str4.size() << " " << str4.length() << " " << str4.capacity() << std::endl;

  std::vector v1{1, 2, 3};

  std::vector<std::string> v2{"abc", "123"};

  v1.push_back(11);
  cout << v1.size() << " " << v1.capacity() << std::endl;
  v1.shrink_to_fit();
  cout << v1.size() << " " << v1.capacity() << std::endl;

  v1.front();
  v1.back();
//  v1.pop_back();

//  v1.erase(v1.begin(), v1.begin() + 2);

  map<string, double> m1;
  m1["abc"] = 1.0;
  m1["abcdef"] = 2.0;
  m1["123"] = 5.0;
  set<float> s1 = {1.0f, 2.0f};
  unordered_map<string, double> m2;
//  struct Arr
//  {
//    int arr[10];
//  };
  // list
  // array
  // deque

  array<int, 10> arr = {};
  for(auto& [k, v] : m1) //structured binding
  {
    cout << k << " : " << v << "\n";
  }

  for(auto& kv : m1)
  {
    cout << kv.first << " : " << kv.second << "\n";
  }


}