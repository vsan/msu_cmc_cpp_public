
#include <iostream>


struct A
{
    A()
    {
        std::cout << "A()" << std::endl;
    }

    ~A()
    {
        std::cout << "~A()" << std::endl;
    }
};

int main()
{
    A *pa = new A;

    delete pa;

    void *pv = ::operator new(sizeof(A));
    A *pb = new (pv) A;

    pb->~A();
    ::operator delete(pb);
}
