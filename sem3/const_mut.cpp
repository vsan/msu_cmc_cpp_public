#include <iostream>

struct S
{
  mutable int x;
};

constexpr int N = 10;
int arr[N];

constexpr int f(int a)
{
  if (a <= 0) return 1;
  return a * f(a - 1);
}

char c[f(N)]{1};

int main()
{
  const S z{10};

  std::cout << z.x << std::endl;

  ++z.x;
  std::cout << z.x << std::endl;

  std::cout << sizeof(c) << std::endl;
}