#include <iostream>
#include <algorithm>
#include <vector>

struct Linear
{
  double a, b;

  double operator()(double x) const
  {
    return a * x + b;
  }
};

std::ostream& operator<<(std::ostream& os, const Linear& l)
{
  os << l.a << " * x + " << l.b;
  return os;
}

struct Sum
{
  int sum;
  Sum() : sum(0) { }
  void operator()(int n) { sum += n; }
};

struct Mult
{
  int mult;
  Mult() : mult(1) { }
  void operator()(int n) { mult *= n; }
};

struct Func
{
  int operator()(int a) const
  {
    return a + 1;
  }
  double operator()(int a, int b) const
  {
    return (a + b + 0.0) / 2;
  }
};

int main() {
  Linear f{2, 1}; // 2x + 1.
  Linear g{-1, 0}; //  -x.

  std::cout << "f(x) = " << f << "\n";

  double f_0 = f(0);
  std::cout << "f(0) = " << f_0 << "\n";
  double f_1 = f(1);
  std::cout << "f(1) = " << f_1 << "\n";

  std::cout << "g(x) = " << g << "\n";
  double g_0 = g(0);
  std::cout << "g(0) = " << g_0 << "\n";

  std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  Sum s = std::for_each(v.begin(), v.end(), Sum());

  std::cout << "sum = " << s.sum << "\n";

  Mult m = std::for_each(v.begin(), v.end(), Mult());
  std::cout << "mult = " << m.mult << "\n";

  Func ff;
  std::cout << ff(10) << std::endl;
  std::cout << ff(1, 2) << std::endl;
}
