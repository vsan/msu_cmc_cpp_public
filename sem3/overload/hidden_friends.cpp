#include <iostream>

namespace A
{
  class X
  {
  public:
    X(int i):data(i){}
  private:
    int data;
    friend bool operator==(X const& lhs, X const& rhs)
    {
      std::cout << "X::operator==\n";
      return lhs.data==rhs.data;
    }
  };

  bool operator==(X const& lhs, X const& rhs);

  class Y
  {
  public:
    Y(int i):data(i){}

    operator X() const
    {
      return X(data);
    }
  private:
    int data;
  };
}

struct X
{
  X(int x){}
  friend void foo(X){};
};

int main()
{
  A::X a(42), b(42);
  if(a==b)
  {
    std::cout << "equal 1\n";
  }
  else
  {
    std::cout << "not equal 1\n";
  }

  A::Y y(99);
  A::X converted = y;

  A::Y aY(1), bY(2);
  if(aY == bY) // ?
  {
    std::cout << "equal 2\n";
  }
  else
  {
    std::cout << "not equal 2\n";
  }

  if(A::X(99) == bY)
  {
    std::cout << "equal 3\n";
  }
  else
  {
    std::cout << "not equal 3\n";
  }

//  X x(42);
//  foo(x); // OK, calls foo defined in friend declaration
//  foo(99); // Error: foo not found, as int is not X
//  ::foo(x); // Error: foo not found as ADL not triggered
}