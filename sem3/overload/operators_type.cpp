#include <iostream>

struct A
{
  inline static int x = 0;
  explicit operator bool () const
  {
    std::cout << "operator bool. x = " << x << std::endl;
    if(x >= 10)
      return false;

    return true;
  }

  operator int *() const
  {
    std::cout << "operator int *" << std::endl;

  }
};

int main()
{
  A a;
  int *z = a;

  while (a)
  {
    A::x++;
  }
}