#include <iostream>
#include <vector>
#include <algorithm>

class A
{
  struct ValueHolder
  {
    int value = 0;
  };

  ValueHolder *data = nullptr;

public:
  A()
  {
    data = new ValueHolder{100};
  }

  ~A()
  {
    delete data;
  }

  const ValueHolder* operator -> () const
  {
    return data;
  }
};


int main()
{
  A p{};
  std::cout << p->value << std::endl;

//  auto f = [p](int a) { return p->value + a; }; double delete !
  auto f = [&p](int a) { return p->value + a; };

  int x = 10;
  std::cout << "lambda " << f(x) << "\n";

  struct Lambda
  {
    int z_;

    Lambda(int z_) : z_(z_) {}
    int operator()(int a) const
    {
      return z_ + a;
    }
  };

  Lambda ff(p->value);
  std::cout << "``lambda`` " << ff(x) << "\n";

  struct Point2D
  {
    int x = 0;
    int y = 0;
  };
  std::vector<Point2D> v = {{10, 2}, {5, 1}, {6, 8}, {3, 10}};

  std::sort(v.begin(), v.end(), [](const Point2D& a, const Point2D& b){return a.x < b.x;});

  for(auto el : v)
  {
    std::cout << "(" << el.x << ", " << el.y << ") ";
  }
  std::cout << "\n";

  struct PointSorter
  {
    bool operator()(const Point2D& a, const Point2D& b) const
    {
      return a.y < b.y;
    }
  };

  std::sort(v.begin(), v.end(), PointSorter());

  for(auto el : v)
  {
    std::cout << "(" << el.x << ", " << el.y << ") ";
  }
  std::cout << "\n";
}