#include <iostream>

struct Point2D
{
//private:
  int x = 0;
  int y = 0;
//public:
  Point2D() = default;
  Point2D(int a_x, int a_y) : x(a_x), y(a_y){};

  const Point2D& operator+=(const Point2D &other)
  {
    x += other.x;
    y += other.y;
    return *this;
  }

  Point2D operator+(const Point2D &other) const
  {
    return Point2D{x + other.x, y + other.y};
  }

//  Point2D operator+(const Point2D &other)
//  {
//    return *this += other;
//  }

  Point2D operator+(int scalar) const
  {
    return Point2D{x + scalar, y + scalar};
  }

  Point2D operator+()
  {
    return *this;
  }

  Point2D operator*(int coeff) const
  {
    return {x * coeff, y * coeff};
  }

  Point2D operator-() const
  {
    return {-1 * x, -1 * y};
  }

  Point2D operator-(const Point2D& other) const
  {
    return {this->x - other.x, this->y - other.y};
  }

//  friend Point2D operator++(Point2D &p, int unused);
//  friend Point2D operator--(Point2D &p, int unused);
//  friend Point2D& operator++(Point2D &p);
//  friend Point2D& operator--(Point2D &p);
//  friend std::ostream& operator<<(std::ostream &os, const Point2D& pt);
};

Point2D operator++(Point2D &p, int unused)
{
  Point2D tmp{p};
  ++p.x;
  ++p.y;
  return tmp;
}

Point2D operator--(Point2D &p, int unused)
{
  Point2D tmp{p};
  --p.x;
  --p.y;
  return tmp;
}

Point2D& operator++(Point2D &p)
{
  ++p.x;
  ++p.y;
  return p;
}

Point2D& operator--(Point2D &p)
{
  --p.x;
  --p.y;
  return p;
}

std::ostream& operator<<(std::ostream &os, const Point2D& pt)
{
  os << "(" << pt.x << ", " << pt.y << ")\n";

  return os;
}


int main()
{
  Point2D a, b, c;

  a = a + 1;
  b = b + 2;
  std::cout << "a: " << a;
  std::cout << "b: " << b;

  c = a + b;
  std::cout << "c: " << c;

  Point2D d = (a++ + --b) + -c;
  std::cout << "d: " << d;

  c = b - ------a;
  std::cout << "c: " << c;
}