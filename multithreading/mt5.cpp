#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <thread>

using namespace std::chrono_literals;

void do_something(int val)
{
  std::stringstream ss;
  ss << "[" << std::this_thread::get_id() << "]: hello " << val << "\n";
  std::cout << ss.str();
}

void do_something_else()
{
  std::stringstream ss;
  ss << "[" << std::this_thread::get_id() << "]: bye\n";
  std::cout << ss.str();
}


std::thread g(std::thread a_t)
{
  a_t.join();
  std::thread t(do_something, 123);
  return t;
}

int main ()
{
  std::thread t1(do_something, 111);
  std::thread t2 = g(std::move(t1));
  t2.join();
//  std::thread t3 = t1;

  std::vector<std::thread> threads;
  for(auto i = 0u; i < 20u; ++i)
  {
    threads.emplace_back(do_something, i);
  }

  for(auto& t: threads)
  {
    t.join();
  }
}