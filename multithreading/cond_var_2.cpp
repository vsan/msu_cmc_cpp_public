#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <thread>
#include <string>
#include <vector>
#include <map>
#include <shared_mutex>
#include <chrono>
#include <queue>

std::mutex mut;
std::queue<std::string> data_queue;
std::condition_variable data_cond;
constexpr int iterations = 32;
void prepare_data()
{
  int i = 1;
  while(i <= iterations)
  {
    auto data = "data item #" + std::to_string(i);
    {
      std::lock_guard<std::mutex> lk(mut);
      data_queue.push(data);
      std::this_thread::sleep_for(std::chrono::nanoseconds(5));
      std::stringstream ss;
      ss << "[tid:" << std::this_thread::get_id() << "] pushed " << data;
      std::cout << ss.str() << std::endl;
    }
    data_cond.notify_one();
    ++i;
  }

  {
    std::lock_guard<std::mutex> lk(mut);
    data_queue.push("stop");
    data_cond.notify_all();
  }
}
void process_data()
{
  while(true)
  {
    std::unique_lock<std::mutex> lk(mut);
    data_cond.wait(
        lk,[]{return !data_queue.empty();});
    auto data = data_queue.front();
    if(data == ("stop"))
    {
      break;
    }
    else
    {
      data_queue.pop();
    }
    lk.unlock();
    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    std::stringstream ss;
    ss << "[tid:" << std::this_thread::get_id() << "] processed " << data;
    std::cout << ss.str() << std::endl;
  }
}

constexpr int nThreads = 32;
int main()
{
//  CreateWorkX();
  std::thread t1(prepare_data);
  std::vector<std::thread> workers;
  for(size_t i = 0; i < nThreads; ++i)
  {
    workers.emplace_back(process_data);
  }

  t1.join();
  for(auto& w : workers)
  {
    w.join();
  }
}