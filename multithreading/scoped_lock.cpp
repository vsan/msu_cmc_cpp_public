#include <iostream>
#include <mutex>
#include <thread>
#include <string>
#include <vector>

struct A
{
private:
  std::string bigData = "Very very very very very very very big data";
  std::mutex m;
public:
  const std::string& getData() const { return bigData; }
  explicit A(std::string d) : bigData(std::move(d)) {}
  A(const A& other)
  {
    bigData = other.bigData;
  }

  friend void swap(A& lhs, A& rhs)
  {
    if(&lhs == &rhs)
      return;

    std::scoped_lock guard(lhs.m, rhs.m);

//  До c++17:
//   std::lock(lhs.m,rhs.m);
//   std::lock_guard<std::mutex> lock_a(lhs.m, std::adopt_lock);
//   std::lock_guard<std::mutex> lock_b(rhs.m, std::adopt_lock);

// unique_lock - можно отложить захват мьютекса до более позднего срока
//    std::unique_lock<std::mutex> lock_a(lhs.m, std::defer_lock);
//    std::unique_lock<std::mutex> lock_b(rhs.m, std::defer_lock);
//    std::lock(lock_a,lock_b);

//  deadlock
//    std::lock_guard<std::mutex> lock_a(lhs.m);
//    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
//    std::lock_guard<std::mutex> lock_b(rhs.m);

    lhs.bigData += "!";
    rhs.bigData += "?";

    swap(lhs.bigData, rhs.bigData);
  }
};

constexpr size_t DATA_SIZE = 8;
void CreateWork()
{
  std::vector<A> data;
  data.reserve(DATA_SIZE);
  for(size_t i = 0; i < DATA_SIZE; ++i)
  {
    data.emplace_back("Very very very very very very very very very very very very very big data #" + std::to_string(i));
  }

//  for(size_t i = 0; i < data.size(); ++i)
//  {
//    std::cout << "i = " << i << " :" << data[i].getData() << std::endl;
//  }
//
//  std::cout << " ******* " << std::endl;
  using namespace std::chrono_literals;
  std::vector<std::thread> threads;
  for(size_t i = 0; i < DATA_SIZE; ++i)
  {
    std::thread t([&data, i](){
        swap(data[i], data[(i + 1) % DATA_SIZE]);
    });
    threads.push_back(std::move(t));
  }

  for(size_t i = 0; i < threads.size(); ++i)
  {
    threads[i].join();
  }

  for(size_t i = 0; i < data.size(); ++i)
  {
    std::cout << "i = " << i << " :" << data[i].getData() << std::endl;
  }

}

int main()
{
  CreateWork();
}