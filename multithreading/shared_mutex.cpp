#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <thread>
#include <string>
#include <vector>
#include <map>
#include <shared_mutex>
#include <chrono>

class X
{
  std::map<std::string, std::string> data_table;
  mutable std::shared_mutex entry_mutex;
public:
  X()
  {
    for(size_t i = 0; i < 1024; ++i)
    {
      data_table[std::to_string(i)] = std::to_string(i);
    }
  }
  std::string find_something(const std::string& k) const
  {
    std::shared_lock<std::shared_mutex> lk(entry_mutex);
    const auto it = data_table.find(k);
    return (it == data_table.end()) ? "not found" : it->second;
  }
  void update_or_add(const std::string& key, const std::string& value)
  {
    std::lock_guard<std::shared_mutex> lk(entry_mutex);
    data_table[key] = value;
  }
};

class Y
{
  std::map<std::string, std::string> data_table;
  mutable std::mutex entry_mutex;
public:
  Y()
  {
    for(size_t i = 0; i < 1024; ++i)
    {
      data_table[std::to_string(i)] = std::to_string(i);
    }
  }
  std::string find_something(const std::string& k) const
  {
    std::lock_guard lk(entry_mutex);
    const auto it = data_table.find(k);
    return (it == data_table.end()) ? "not found" : it->second;
  }
  void update_or_add(const std::string& key, const std::string& value)
  {
    std::lock_guard lk(entry_mutex);
    data_table[key] = value;
  }
};


constexpr size_t DATA_SIZE = 256;
void CreateWorkX()
{
  X resource;
  using namespace std::chrono_literals;
  std::vector<std::thread> threads;

  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};
  for(size_t i = 1; i <= DATA_SIZE; ++i)
  {
    if(i % 32 != 0)
    {
      std::thread t([i, &resource]() {
          std::stringstream ss;
          ss << "[tid:" << std::this_thread::get_id() << "] ";
          ss << resource.find_something("abc");
//          std::cout << ss.str() << "\n";
      });
      threads.push_back(std::move(t));
    }
    else
    {
      std::thread t([i, &resource]() {
          std::stringstream ss;
          ss << "[tid:" << std::this_thread::get_id() << "] ";
          resource.update_or_add("abc", std::to_string(i));
          ss << "Added record\n";
//          std::cout << ss.str();
      });
      threads.push_back(std::move(t));
    }

  }

  for(size_t i = 0; i < threads.size(); ++i)
  {
    threads[i].join();
  }

  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "Duration = "
            << std::chrono::duration_cast<std::chrono::microseconds>(dur).count() << " microseconds\n";
}

void CreateWorkY()
{
  Y resource;
  using namespace std::chrono_literals;
  std::vector<std::thread> threads;

  std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};
  for(size_t i = 1; i <= DATA_SIZE; ++i)
  {
    if(i % 32 != 0)
    {
      std::thread t([i, &resource]() {
          std::stringstream ss;
          ss << "[tid:" << std::this_thread::get_id() << "] ";
          ss << resource.find_something("abc");
//          std::cout << ss.str() << "\n";
      });
      threads.push_back(std::move(t));
    }
    else
    {
      std::thread t([i, &resource]() {
          std::stringstream ss;
          ss << "[tid:" << std::this_thread::get_id() << "] ";
          resource.update_or_add("abc", std::to_string(i));
          ss << "Added record\n";
//          std::cout << ss.str();
      });
      threads.push_back(std::move(t));
    }

  }

  for(size_t i = 0; i < threads.size(); ++i)
  {
    threads[i].join();
  }

  std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
  auto dur = finish - start;
  std::cout << "Duration = "
            << std::chrono::duration_cast<std::chrono::microseconds>(dur).count() << " microseconds\n";
}

int main()
{
//  CreateWorkX();
  CreateWorkY();
}