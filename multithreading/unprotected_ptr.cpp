#include <iostream>
#include <mutex>
#include <thread>
#include <string>
#include <vector>

class some_data
{
  int a = 0;
  std::string b = "abc";
public:
  void do_something()
  {
    b += std::to_string(a) + ",";
    a++;
  }

  void breakstuff()
  {
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(200ns);
    b[b.size() - 1] = '!';
  }

  const std::string& GetStr() const
  {
    return b;
  }
};


class data_wrapper
{
private:
  some_data data;
  std::mutex m;
public:
  template<typename Function>
  void process_data(Function func)
  {
    std::lock_guard<std::mutex> l(m);
    func(data);
  }
};

some_data* unprotected;

void malicious_function(some_data& protected_data)
{
  protected_data.do_something();
  unprotected = &protected_data;
}

void CreateWork(data_wrapper &data)
{
  std::vector<std::thread> threads;
  for(int i = 0; i < 128; ++i)
  {
    std::thread t([&data](){
        data.process_data(malicious_function);
        unprotected->breakstuff();
    });
    threads.push_back(std::move(t));
  }

  for(int i = 0; i < 128; ++i)
  {
    threads[i].join();
  }
}

int main()
{
  data_wrapper x;
  CreateWork(x);
  std::cout << unprotected->GetStr() << "\n";
}