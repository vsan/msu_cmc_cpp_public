#include <string>
#include <iostream>
#include <thread>


void exc()
{
  throw std::runtime_error("exception!");
}

void do_something()
{
  std::cout << "[" << std::this_thread::get_id() << "]: hello\n";
}

void do_something_else()
{
  std::cout << "[" << std::this_thread::get_id() << "]: bye\n";
}

class background_task
{
public:
  void operator()() const
  {
    do_something();
    do_something_else();
  }
};

struct A
{
  int data = 0;
};

void update_data(int i, A& a)
{
  a.data = i;
}

void foo(int i)
{
  A a;
  std::thread t(update_data, i, std::ref(a));
  t.join();

  std::cout << "A::data = " << a.data << std::endl;
}


int main ()
{
  foo(11);
}