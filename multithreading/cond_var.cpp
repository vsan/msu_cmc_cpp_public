#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <thread>
#include <string>
#include <vector>
#include <map>
#include <shared_mutex>
#include <chrono>
#include <queue>

std::mutex mut;
std::queue<std::string> data_queue;
std::condition_variable data_cond;
constexpr int iterations = 128;
void prepare_data()
{
  int i = 1;
  while(i <= iterations)
  {
    auto data = "data item #" + std::to_string(i);
    {
      std::lock_guard<std::mutex> lk(mut);
      data_queue.push(data);
      std::this_thread::sleep_for(std::chrono::nanoseconds(5));
      std::stringstream ss;
      ss << "[tid:" << std::this_thread::get_id() << "] pushed " << data;
      std::cout << ss.str() << std::endl;
    }
    data_cond.notify_one();
    ++i;
  }
}
void process_data()
{
  while(true)
  {
    std::unique_lock<std::mutex> lk(mut);
    data_cond.wait(
        lk,[]{return !data_queue.empty();});
    auto data = data_queue.front();
    data_queue.pop();
    lk.unlock();
    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    std::stringstream ss;
    ss << "[tid:" << std::this_thread::get_id() << "] processed " << data;
    std::cout << ss.str() << std::endl;
    if(data == ("data item #" + std::to_string(iterations)))
      break;
  }
}


int main()
{
//  CreateWorkX();
  std::thread t1(prepare_data);
  std::thread t2(process_data);

  t1.join();
  t2.join();
}