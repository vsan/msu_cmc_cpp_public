#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;

class X
{
private:
  std::ofstream connection;
  std::once_flag connection_init_flag;
  void open_connection()
  {
    std::cout << "open_connection\n";
    connection.basic_ios<char>::rdbuf(std::cout.rdbuf());
  }
public:
  void write1(const std::string& data)
  {
    std::call_once(connection_init_flag, &X::open_connection, this);
    connection << data;
  }

  void write2(const std::string& data)
  {
    std::call_once(connection_init_flag, &X::open_connection, this);
    connection << "uuuooooooooooh: " << data;
  }
  void flush()
  {
    connection.flush();
  }
};

void CreateWork()
{
  X output;
  std::vector<std::thread> threads;
  for(size_t i = 0; i < 8; ++i)
  {
    std::thread t([i, &output](){
      std::stringstream ss;
      ss << "[tid:" << std::this_thread::get_id() << "]\n";
      if(i % 2 == 0)
        output.write1(ss.str());
      else
        output.write2(ss.str());
    });
    threads.push_back(std::move(t));
  }

  for(size_t i = 0; i < 8; ++i)
  {
    threads[i].join();
  }

  output.flush();
}


int main ()
{
  CreateWork();
}