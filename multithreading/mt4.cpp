#include <string>
#include <iostream>
#include <sstream>
#include <thread>

using namespace std::chrono_literals;

void do_something(int val)
{
  std::stringstream ss;
  ss << "[" << std::this_thread::get_id() << "]: hello " << val << "\n";
  std::cout << ss.str();
}

void do_something_else()
{
  std::stringstream ss;
  ss << "[" << std::this_thread::get_id() << "]: bye\n";
  std::cout << ss.str();
}


int main ()
{
  std::thread t1(do_something, 111);
  std::thread t2 = std::move(t1);
  t1 = std::thread(do_something_else);
  std::thread t3;
  t3 = std::move(t2);

  t1.join();
//  t2.join();
  t3.join();
}