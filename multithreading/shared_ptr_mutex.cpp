#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <future>
#include <vector>
#include <chrono>
#include <thread>
#include <random>
#include <mutex>

struct Resource
{
  explicit Resource(const std::string &d) : data(d)
  {
    std::cout << "Resource()\n";
  }

  ~Resource()
  {
    std::cout << "~Resource()\n";
  }

  const std::string& GetRes() const
  {
    return data;
  }

  std::string& GetRes()
  {
    return data;
  }
private:
  std::string data;
};


int getRandomTime()
{
  static std::random_device r;
  static std::default_random_engine e1(r());
  static std::uniform_int_distribution<int> uniform_dist(100, 500);
  return uniform_dist(e1);
}

std::mutex some_mutex;
void DoWork(std::shared_ptr<Resource> ptr)
{
  std::stringstream ss;
  ss << std::this_thread::get_id();
  std::string tid_str = ss.str();
  ss.str(std::string());
  ss << "[tid: " << tid_str << "]: " << "Doing work with resource: " << ptr->GetRes()
     << "; use_count =  " << ptr.use_count() << "\n";
  std::cout << ss.str();

  {
    std::lock_guard<std::mutex> guard(some_mutex);
    ptr->GetRes().append(tid_str);
  }

  ptr->GetRes().push_back('!');
  std::this_thread::sleep_for(std::chrono::milliseconds(getRandomTime()));
  ss.str(std::string());
  ss << "[tid: " << tid_str << "]: " << "Done!\n";
  std::cout << ss.str();
}

static constexpr int WORKGROUP_SIZE = 128;
std::vector<std::future<void>> CreateWork()
{
  std::vector<std::future<void>> work;
  work.reserve(WORKGROUP_SIZE);

  auto resource = std::make_shared<Resource>("VeryImportantResource");

  for(int i = 0; i < WORKGROUP_SIZE; ++i)
  {
    work.push_back(std::async(DoWork, resource));
  }

  return work;
}

int main()
{
  auto futures = CreateWork();
}