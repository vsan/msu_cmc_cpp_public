#include <iostream>
#include <thread>
#include <sstream>
#include <vector>
#include <numeric>
#include <chrono>
#include <execution>

using namespace std::chrono_literals;

template<typename Iterator, typename T>
struct accumulate_block
{
  void operator()(Iterator first, Iterator last, T& result)
  {
    result = std::accumulate(first, last, result);
  }
};

template<typename T, typename Iterator>
T parallel_accumulate(Iterator first, Iterator last, T init)
{
  auto length = std::distance(first, last);

  if(length < 0)
    return 0;
  else if(!length)
      return init;

  const uint32_t min_per_thread = 32;
  const uint32_t max_threads = (length + min_per_thread - 1) / min_per_thread;
  const uint32_t hardware_threads = std::thread::hardware_concurrency();
  const uint32_t num_threads = std::min( hardware_threads != 0 ? hardware_threads : 2, max_threads);
  const uint64_t block_size = length / num_threads;

  std::cout << "hardware_threads = " << hardware_threads << "\n"
            << "num_threads = " << num_threads << "\n"
            << "block_size = " << block_size << "\n";

  std::vector<T> results(num_threads);
  std::vector<std::thread> threads(num_threads-1);
  Iterator block_start = first;

  for(uint32_t i = 0; i < (num_threads - 1); ++i)
  {
    Iterator block_end = block_start;
    std::advance(block_end, block_size);
    threads[i]=std::thread(
        accumulate_block<Iterator,T>(),
        block_start, block_end, std::ref(results[i]));
    block_start=block_end;
  }

  accumulate_block<Iterator, T>()(block_start, last, results[num_threads-1]);
  for(auto& entry: threads)
    entry.join();
  return std::accumulate(results.begin(), results.end(), init);
}

int main()
{
  std::vector<int> v(72'000'000);
  std::iota(v.begin(), v.end(), 1);

  {
    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

    auto res = parallel_accumulate<uint64_t>(v.begin(), v.end(), 0);

    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;

    std::cout << "accumulate (parallel) = " << v.size() << ": " << res;
    std::cout << " ; duration = "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(dur).count() << " ns" << std::endl;
  }

  {
    uint64_t res2 = 0u;

    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

    res2 = std::accumulate(v.begin(), v.end(), res2);

    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;

    std::cout << "accumulate (sequential) = " << v.size() << ": " << res2;
    std::cout << " ; duration = "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(dur).count() << " ns" << std::endl;
  }

  {
    uint64_t res2 = 0u;

    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

    res2 = std::reduce(std::execution::par, v.cbegin(), v.cend());

    std::chrono::steady_clock::time_point finish{std::chrono::steady_clock::now()};
    auto dur = finish - start;

    std::cout << "accumulate (exec) = " << v.size() << ": " << res2;
    std::cout << " ; duration = "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(dur).count() << " ns" << std::endl;
  }
}