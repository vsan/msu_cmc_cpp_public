#include <string>
#include <iostream>
#include <thread>


void exc()
{
  throw std::runtime_error("exception!");
}

void do_something()
{
  std::cout << "[" << std::this_thread::get_id() << "]: hello\n";
}

void do_something_else()
{
  std::cout << "[" << std::this_thread::get_id() << "]: bye\n";
}

class background_task
{
public:
  void operator()() const
  {
    do_something();
    do_something_else();
  }
};

class thread_guard
{
  std::thread& t;
public:
  explicit thread_guard(std::thread& t_): t(t_) {}
  ~thread_guard()
  {
    if(t.joinable())
    {
      t.join();
    }
  }
  thread_guard(thread_guard const&) = delete;
  thread_guard& operator=(thread_guard const&) = delete;
};

void foo()
{
  std::thread my_t2([]
                    {
                        do_something();
                        do_something_else();
                    });
  thread_guard g(my_t2);
  exc();
}

void bar()
{
  std::cout << "oops" << std::endl;
}



int main ()
{
  try
  {
    foo();
  }
  catch(...)
  {
    std::cout << "caught" << std::endl;
  }
  bar();
}