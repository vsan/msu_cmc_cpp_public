#include <string>
#include <iostream>
#include <sstream>
#include <thread>

using namespace std::chrono_literals;


void do_something_else()
{
  std::stringstream ss;
  ss << "[" << std::this_thread::get_id() << "]: bye\n";
  std::cout << ss.str();
}

struct A
{
  int data = 0;
  void do_something(int val) const
  {
    std::stringstream ss;
    ss << "[" << std::this_thread::get_id() << "]: hello " << val << "\n";
    std::cout << ss.str();
  }
};

void foo(std::unique_ptr<A> aPtr)
{
  std::stringstream ss;
  std::cout << "A::data = " << aPtr->data << std::endl;
  std::cout << ss.str();
}


int main ()
{
  A a1;
  A a2;
  a1.data = 11;
  a2.data = 22;

  std::thread t1(&A::do_something, &a1, 111);
  std::thread t2(&A::do_something, &a2, 222);

  auto aPtr = std::make_unique<A>();
  aPtr->data = 333;

  std::thread t3(foo, std::move(aPtr));

  std::cout << "aPtr = " << aPtr.get() << std::endl;

  t1.join();
  t2.join();
  t3.join();
}