#include <iostream>
#include <iterator>
#include <vector>
#include <list>

template<typename ... T>
auto sum(const T& ... sum)
{
  return (sum + ...);
}

template <typename T1>
auto sum2(const T1& x1)
{
  return x1;
}

template <typename T1, typename ... T>
auto sum2(const T1& x1, const T& ... x)
{
  return x1 + sum2(x ...);
}


int main()
{
  std::cout << sum(1, 3, 5) << "\n";
  std::cout << sum(1, 3.14, 54, 100l) << "\n";

  std::cout << sum2(1, 3, 5) << "\n";
  std::cout << sum2(1, 3.14, 54, 100l) << "\n";
}
