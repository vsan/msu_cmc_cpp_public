#include <iostream>
#include <iterator>
#include <vector>
#include <list>


template <typename T>
T half(T x)
{
  return x / 2;
}

template <typename U, typename V>
U half2(V x)
{
  return x / 2;
}

int main()
{
  std::cout << half<int>(5) << "\n";
  std::cout << half<double>(5) << "\n";

  auto x = half(5);
  auto y = half(5.0);

  std::cout << x << "\n";
  std::cout << y << "\n";

  long l = 12345l;
  unsigned u = 1234u;
  auto val = half(l + u);

  std::cout << half2<int>(5.0) << "\n";
  std::cout << half2<double>(5) << "\n";
}
