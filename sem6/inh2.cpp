#include <iostream>
#include <iterator>
#include <vector>
#include <list>

class Base
{

};

class Derived : public Base
{

};

int main()
{
  Derived *d = new Derived;
  Base *b = d;

  Base *b2 = new Derived;
  Derived *d2 = static_cast<Derived*>(b2);
}
