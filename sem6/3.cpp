#include <algorithm>
#include <list>
#include <vector>
#include <iostream>
#include <numeric>
#include <random>
#include <functional>

template<typename BidirectionalIterator, typename Predicate =
std::less<typename std::iterator_traits<BidirectionalIterator>::value_type>>
void
insertion_sort(BidirectionalIterator first, BidirectionalIterator last,
               Predicate pred = {}) {
  if(first != last) {
    auto it = first;
    while(++it != last) {
      auto it2 = it;
      while(it2 != first) {
        auto it3 = it2;
        --it3;
        if(pred(*it2, *it3)) {
          std::swap(*it2, *it3);
        } else {
          break;
        }
        --it2;
      }
    }
  }
}

int main()
{

}