#include <iostream>
#include <iterator>
#include <vector>
#include <list>

class Container : private std::vector<int>
{
public:
  using std::vector<int>::size;
//  size_t size()const {return m_vec.size();}

private:
  std::vector<int> m_vec;
};

class StringNonCopyable : public std::string
{
public:
  using std::string::string;
  StringNonCopyable(std::string&& other) : std::string(std::move(other)) {}
  StringNonCopyable(const StringNonCopyable&) = delete;
  StringNonCopyable(StringNonCopyable&&) = default;
  StringNonCopyable& operator=(const StringNonCopyable&) = delete;
  StringNonCopyable& operator=(StringNonCopyable&&) = default;
//  size_t size()const {return m_vec.size();}
};

int main()
{
  Derived *d = new Derived;
  Base *b = d;

  Base *b2 = new Derived;
  Derived *d2 = static_cast<Derived*>(b2);
}
