#include <iostream>
#include <iterator>
#include <vector>
#include <list>

struct Figure
{
  Figure(const std::string &t)
  {
    std::cout << "Figure() : " << t << "\n";
  }
};

struct Triangle : public Figure
{
  Triangle(const std::string &t) : Figure(t)
  {
    std::cout << "Triangle() : " << t << "\n";
  }
};

struct Mesh
{
  Mesh() : t2("mt2"), t1("mt1")
  {

  }
private:
  Triangle t2;
  Triangle t1;
};


int main()
{
 Triangle t1("t1");
 Triangle t2("t2");

 Mesh m;
}
