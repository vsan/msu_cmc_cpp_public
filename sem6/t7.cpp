#include <iostream>
#include <iterator>
#include <vector>
#include <list>


//полная специализация или явная специализация

template<typename N, typename D>
class Ratio
{
  N numerator;
  D denominator;
public:
  Ratio() : numerator(), denominator() {}
  Ratio(const N& num, const D& denom) : numerator(num), denominator(denom){}

  operator double() const
  {
    std::cout << "Ratio<N, D>: ";
    return double(numerator) / double(denominator);
  }
};

template<>
class Ratio<double, double>
{
  double ratio;
public:
  Ratio() : ratio() {}
  template<typename N, typename D>
  Ratio(const N& num, const D& denom) : ratio(num / denom){}

  operator double() const
  {
    std::cout << "Ratio<double, double>: ";
    return ratio;
  }
};

template<>
Ratio<float, float>::operator double() const
{
  std::cout << "Ratio<N, D>::operator double() specialization ";
  return double(numerator) / double(denominator);
}

template <typename T>
T f(T x)
{
  std::cout << "T f(T x): ";
  return x + 1;
}

template <>
double f<double>(double x)
{
  std::cout << "double f<double>(double x): ";
  return x / 2;
}

int main()
{

  Ratio r1(10, 1.5);
  Ratio<double, int> r2(10, 1.5);

  std::cout << r1 << "\n";
  std::cout << r2 << "\n";

  std::cout << Ratio(10.0, 1.5) << "\n";
  std::cout << Ratio(10.0f, 1.5f) << "\n";

  std::cout << f(5) << "\n";
  std::cout << f(5.0) << "\n";
}
