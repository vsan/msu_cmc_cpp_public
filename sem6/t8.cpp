#include <iostream>
#include <iterator>
#include <vector>
#include <list>


//частичная специализация

template<typename N, typename D>
class Ratio
{
  N numerator;
  D denominator;
public:
  Ratio() : numerator(), denominator() {}
  Ratio(const N& num, const D& denom) : numerator(num), denominator(denom){}

  operator double() const
  {
    std::cout << "Ratio<N, D>: ";
    return double(numerator) / double(denominator);
  }
};

template<typename N>
class Ratio<N, double>
{
  double ratio;
public:
  Ratio() : ratio() {}
  Ratio(const N& num, const double denom) : ratio(double(num) / denom){}

  operator double() const
  {
    std::cout << "Ratio<N, double>: ";
    return ratio;
  }
};

template<typename D>
class Ratio<double, D>
{
  double ratio;
public:
  Ratio() : ratio() {}
  Ratio(const double num, const D& denom) : ratio(num / double(denom)){}

  operator double() const
  {
    std::cout << "Ratio<double, D>: ";
    return ratio;
  }
};

template<>
class Ratio<double, double>
{
  double ratio;
public:
  Ratio() : ratio() {}
  template<typename N, typename D>
  Ratio(const N& num, const D& denom) : ratio(num / denom){}

  operator double() const
  {
    std::cout << "Ratio<double, double>: ";
    return ratio;
  }
};

template<typename N, typename D>
class Ratio<N*, D*>
{
  const N* numerator;
  const D* denominator;
public:
  Ratio(N* num, D* denom) : numerator(num), denominator(denom){}

  operator double() const
  {
    std::cout << "Ratio<N*, D*>: ";
    return double(*numerator) / double(*denominator);
  }
};

template <typename T> class Value;

template <typename T> class Value<T*>
{
  T val;
public:
  explicit Value(T* ptr) : val(*ptr)
  {
    std::cout << "Value<T*>\n";
  }
};

template <typename T> class Value<T&>
{
  T val;
public:
  explicit Value(T& ptr) : val(ptr)
  {
    std::cout << "Value<T&>\n";
  }
};


int main()
{

  Ratio r1(10, 1.5);
  Ratio<double, int> r2(10, 1.5);

  std::cout << r1 << "\n";
  std::cout << r2 << "\n";

  std::cout << Ratio(10.0, 1.5) << "\n";
  std::cout << Ratio(10.0f, 1.5f) << "\n";

  int i = 10;
  double j = 1.5;
  std::cout << Ratio(&i, &j) << "\n";

  int *ptr = &i;
  int& ref = i;

  Value<int*> val1(ptr);
  Value<int&> val2(ref);
//  Value<int> val3(i);
}
